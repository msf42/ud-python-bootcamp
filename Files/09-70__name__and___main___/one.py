#one.py

def func():
    print("FUNC() IN ONE.PY")

print("TOP LEVEL IN ONE.PY")

if __name__ == '__main__':
	print('ONE.PY is being run directly!')	# if one.py is called directly from command line, this will print
else:
	print('ONE.PY has been imported!')
