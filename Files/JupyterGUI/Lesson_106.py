#!/usr/bin/env python
# coding: utf-8

# In[1]:


from ipywidgets import interact, interactive, fixed
import ipywidgets as widgets


# In[2]:


def func(x):
    return x


# In[3]:


interact(func, x=10)


# In[4]:


def func(x):
    return x**2


# In[5]:


interact(func, x=10)


# In[6]:


interact(func,x=True)


# In[8]:


def func(x):
    return x


# In[11]:


interact(func,x='Hello')


# In[12]:


@interact(x=True, y=1.0)
def g(x,y):
    return(x,y)


# In[13]:


@interact(x=True, y=fixed(1.0))
def g(x,y):
    return(x,y)


# In[14]:


interact(func,x=1)


# In[15]:


interact(func,x=10) # slider size is x*3


# In[21]:


interact(func,x=widgets.IntSlider(min=-100,max=100,step=1,value=0))


# In[22]:


interact(func,x=(-100,100,1)) # abbreviated


# In[23]:


@interact(x=(0.0,20.0,0.5))
def h(x=5.0):
    return x


# In[24]:


interact(func,x=['hello', 'option2', 'option3'])


# In[25]:


interact(func,x={'one':10, 'two':20})


# In[26]:


from IPython.display import display

def f(a,b):
    display(a+b)
    return a+b


# In[27]:


w = interactive(f,a=10,b=20)


# In[28]:


type(w)


# In[29]:


w.children


# In[30]:


display(w)


# In[ ]:




