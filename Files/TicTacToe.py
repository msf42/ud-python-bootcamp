# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

Top_L = ("_")

Top_C = ("_")

Top_R = ("_")

Mid_L= ("_")

Center = ("_")

Mid_R = ("_")

Bot_L = ("_")

Bot_C = ("_")

Bot_R = ("_")
print(f'| {Top_L} | {Top_C} | {Top_R} |\n| {Mid_L} | {Center} | {Mid_R} |\n| {Bot_L} | {Bot_C} | {Bot_R} |')

def tictac():
    print("Welcome to Tic-Tac-Toe!")
    print("You already know the rules, so here are the\n instructions for python version")
    print("Here is the blank board:")
    print(f'| {Top_L} | {Top_C} | {Top_R} |\n| {Mid_L} | {Center} | {Mid_R} |\n| {Bot_L} | {Bot_C} | {Bot_R} |')
    print("Here are the codes for each position.\n This is what you will enter\n when asked for your move.")
    print(f'| 1 | 2 | 3 |\n| 4 | 5 | 6 |\n| 7 | 8 | 9 |')
    print("Player 1 will be 'X'. Player 2 will be 'O'")
    print("Let's begin!")

Top_L = "_"    
def player1():
    play1move = int(input("Player 1: What is your move?"))
    if play1move == 1:
        while Top_L != "_":
            play1move = int(input("That space is taken! Please choose another:"))
        Top_L = "X"

player1()
    