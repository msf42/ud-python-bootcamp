class Account:

	def __init__(self, owner, balance):
		self.owner = owner
		self.balance = balance
	
	def __str__(self):
		return f"Account Owner:		{self.owner}\nAccount Balance:	{self.balance}"
	
	def make_deposit(self, deposit):
		self.deposit = deposit
		self.balance = self.balance + self.deposit
		print(f"Deposit Accepted.\nNew Balance = {self.balance}")
		
	def make_withdraw(self, withdraw):
		self.withdraw = withdraw
		if self.withdraw > self.balance:
			print(f"Funds Unavailable! Your balance is only {self.balance}")
		else:
			self.balance = self.balance - self.withdraw
			print(f"Withdraw of {self.withdraw} accepted\nNew Balance = {self.balance}")


acct1 = Account('Jose', 100)

print(acct1)

acct1.make_deposit(50)

acct1.make_withdraw(100)

acct1.make_deposit(500)

acct1.make_withdraw(10)

acct1.make_withdraw(1000)
