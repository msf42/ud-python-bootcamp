import math

class Line:

	def __init__(self,coor1,coor2):
		self.coor1 = coor1
		self.coor2 = coor2
		print(f"coordinate1 = {coor1}")
		print(f"coordinate2 = {coor2}")

	def distance(self):
		self.xdiff = self.coor1[0] - self.coor2[0]
		self.ydiff = self.coor1[1] - self.coor2[1]
		self.dist = math.sqrt((self.xdiff **2) + (self.ydiff ** 2))
		print(f"distance = {self.dist}")
		
	def slope(self):
		self.rise = self.coor2[1] - self.coor1[1]
		self.run = self.coor2[0] - self.coor1[0]
		self.slope = self.rise/self.run
		print(f"slope = {self.slope}")

li = Line((3, 2), (8, 10))

li.distance()

li.slope()
