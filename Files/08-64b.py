class Cylinder:

	pi = 3.14159
	
	def __init__(self,height=1,radius=1):
		self.height = height
		self.radius = radius
	
	def volume(self):
		self.volume = (self.pi * self.radius ** 2) * self.height
		print(f"Given a height of {self.height} and a radius of {self.radius}, the volume is {self.volume}")
	
	def surface_area(self):
		self.area = ((self.pi * (self.radius ** 2)) * 2) + (self.height * (self.pi * self.radius * 2))
		print(f"Given a height of {self.height} and a radius of {self.radius}, the surface area is {self.area}")

c = Cylinder()

c.volume()

c.surface_area()

c = Cylinder(7, 12)

c.volume()

c.surface_area()
