# Course Notes for Udemy Complete Python Bootcamp

These are my notes for [Complete Python Bootcamp course](https://www.udemy.com/course/complete-python-bootcamp/) on [Udemy](www.udemy.com)

This was my first Udemy course and it was excellent! Jose Portilla is a great instructor.

![image](Python_Bootcamp.jpg)

---
