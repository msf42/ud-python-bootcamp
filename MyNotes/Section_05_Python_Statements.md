# Section 05: Python Statements

## 33 - If, Elif, and Else Statements

### Control Flow

Allows us to use logic to execute code only when we want to make use of colons and indentation (white space)
Critical for Python

### Keywords

- **if** some condition:
Execute some code
- **elif** some other condition:
do something different
- **else:**
do something else

```py
  In [1]: if True:
    ...:     print('ITS TRUE')
  ITS TRUE
  
  In [2]: hungry = True
  
  In [3]: if hungry:
    ...:     print('Feed me!')
  Feed me!
  
  In [4]: if hungry:
    ...:     print('Feed me!')
    ...: else:
    ...:     print("I'm not hungry")
  Feed me!
  
  In [6]: hungry = False
  
  In [7]: if hungry:
    ...:     print('Feed me!')
    ...: else:
    ...:     print("Im not hungry")
  Im not hungry
  
  In [8]: loc = 'Bank'
  
  In [9]: if loc == 'Auto Shop':
    ...:     print("Cars are cool!")
    ...: else:
    ...:     print('I do not know much')
  I do not know much
  
  In [10]: if loc == 'Auto Shop':
      ...:     print("Cars are cool!") # after hitting return, backspace before elif
      ...: elif loc == 'Bank':
      ...:     print('Money is cool')
      ...: else:
      ...:     print('I do not know much')
  Money is cool
  ```

---

## 34 - For Loops

### Iterable

Many objects in Python are **iterable**, meaning we can iterate over every element in the object
Such as:

- Every element in a list
- Every character in a string
- Every key in a dictionary

#### We can use For Loops to execute a block of code for every iteration

#### Syntax

```py
  In [1]: my_iterable = [1, 2, 3]
  # we chose the name and list, but it could be anything
  
  In [2]: for item_name in my_iterable:
    ...:     print(item_name)
    ...:
  1
  2
  3
  
  
  In [1]: mylist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
  In [2]: for num in mylist:
    ...:     print(num)
  1
  2
  3
  4
  5
  6
  7
  8
  9
  10
  
  In [3]: for fuck in mylist:  # we can call the item anything we want
    ...:     print(fuck)
  1
  2
  3
  4
  5
  6
  7
  8
  9
  10
  
  In [4]: for fuck in mylist:
    ...:     print('hello')  # we can also have it print/do anything based on those items
  hello
  hello
  hello
  hello
  hello
  hello
  hello
  hello
  hello
  hello
  
  In [5]: for num in mylist:
    ...:     # Check for even
    ...:     if num %2 ==0:  # if the remainder is 0 after dividing by 2
    ...:         print(num)
  2
  4
  6
  8
  10
  
  In [8]: for num in mylist:
    ...:     # Check for even
    ...:     if num %2 ==0:
    ...:         print(num)
    ...:     else:
    ...:         print(f'Odd Number: {num}')  # adding an else statement
  Odd Number: 1
  2
  Odd Number: 3
  4
  Odd Number: 5
  6
  Odd Number: 7
  8
  Odd Number: 9
  10
  
  In [14]: list_sum = 0
      ...: for num in mylist:
      ...:     list_sum = list_sum + num  # for each number, we add it to the total
      ...: print(list_sum)
  55
  
  In [15]: list_sum = 0
      ...: for num in mylist:
      ...:     list_sum = list_sum + num
      ...:     print(list_sum)  # since the print is within the loop, it prints each iteration
  1
  3
  6
  10
  15
  21
  28
  36
  45
  55
  
  In [16]: mystring = 'Hello World'
  
  In [17]: for letter in mystring:
      ...:     print(letter)
  H
  e
  l
  l
  o
  
  W
  o
  r
  l
  d
  
  In [18]: tup = (1, 2, 3)
  
  In [19]: for item in tup:  # a tuple
      ...:     print('fuck')
  fuck
  fuck
  fuck
  
  In [20]: mylist = [(1, 2), (3, 4), (5, 6), (7, 8)]  # a list of tuples
  
  In [21]: len(mylist)
  Out[21]: 4  # note only 4 items
  
  In [22]: for i in mylist:
      ...:     print(i)  # this prints the entire item
  (1, 2)
  (3, 4)
  (5, 6)
  (7, 8)
  
  In [23]: for (a, b) in mylist:  # this prints them separately
      ...:     print(a)
      ...:     print(b)
  1
  2
  3
  4
  5
  6
  7
  8
  
  In [24]: for a, b in mylist:      # the parentheses are unnecessary
      ...:     print(b)              # here we only print the second item in each tuple
  2
  4
  6
  8
  
  In [25]: mylist = [(1, 2, 3), (4, 5, 6), (7, 8, 9)]
  
  In [26]: for i in mylist:
      ...:     print(i)
  (1, 2, 3)
  (4, 5, 6)
  (7, 8, 9)
  
  In [27]: for a, b, c in mylist:
      ...:     print(b)
  2
  5
  8
  
  In [28]: for a, b, c in mylist:
      ...:     print(b, a, c)  # we can rearrange them
  2 1 3
  5 4 6
  8 7 9
  
  In [29]: d = {'k1':1, 'k2':2, 'k3': 3}
  
  In [30]: for i in d:
      ...:     print(i)          # by default, only the keys are printed
  k1
  k2
  k3
  
  In [32]: for i in d.items():  # this will print both
      ...:     print(i)
  ('k1', 1)
  ('k2', 2)
  ('k3', 3)
  
  In [33]: for k, v in d.items():
      ...:     print(v)  # here we only print the value associated with it
  1
  2
  3
  
  In [34]: for i in d.values():  # values only prints the value
      ...:     print(i)
  1
  2
  3
  ```

---

## 35 - While Loops

### While Loops continue to execute a block of code while some condition remains true

#### Syntax:

- **while** some_boolean_condition
  - do something
- **else:**
  - do something else

```py
  In [1]: x = 0
  
  In [2]: while x < 5:
    ...:     print(f'The current value of x is {x}')
    ...:     x = x + 1
  The current value of x is 0
  The current value of x is 1
  The current value of x is 2
  The current value of x is 3
  The current value of x is 4
  
  In [3]: while x < 5:
    ...:     print(f'The current value of x is {x}')
    ...:     x = x + 1
    ...: else:
    ...:     print("X is not less than 5")
  X is not less than 5  # the while loop never needed to execute
  
  In [4]: x = 0  # resetting x to 0
  
  In [5]: while x < 5:
    ...:     print(f'The current value of x is {x}')
    ...:     x = x + 1
    ...: else:
    ...:     print("X is not less than 5")
  The current value of x is 0
  The current value of x is 1
  The current value of x is 2
  The current value of x is 3
  The current value of x is 4
  X is not less than 5
  ```

### Break, Continue, Pass

- **break:** breaks out of the current closest running loop
- **continue:** goes to the top of the closest enclosing loop
- **pass:** does nothing

```py
  In [1]: x = [1, 2, 3]
  
  In [2]: for i in x:  #  This will give an error
    ...:     # comment
    File "<ipython-input-2-5f94d720fc95>", line 2
      # comment
              ^
  SyntaxError: unexpected EOF while parsing  # unexpected End Of File
  
  In [4]: for i in x:
    ...:     # comment
    ...:     pass  # the "pass" will allow it to leave the loop and continue
    ...: print('end of my script')
  end of my script
  
  In [5]: mystring = 'Sammy'
  
  In [7]: for l in mystring:
    ...:     print(l)
  S
  a
  m
  m
  y
  
  In [8]: for l in mystring:
    ...:     if l == 'a':
    ...:         continue  # this tells it to "just keep moving" if it encounters an "a"
    ...:     print(l)
  S
  m
  m
  y
  
  In [10]: for l in mystring:
      ...:     if l == 'a':
      ...:         break                # this tells it to stop once it encounters an "a"
      ...:     print(l)
  S
  
  In [11]: x = 5
  
  In [12]: while x < 20:
      ...:     print(x)
      ...:     x+=1
  5
  6
  7
  8
  9
  10
  11
  12
  13
  14
  15
  16
  17
  18
  19
  
  In [14]: x = 5
  
  In [15]: while x < 20:
      ...:     if x == 12:
      ...:         break                # again, this tells it to stop if it reaches 12
      ...:     print(x)
      ...:     x+=1
      ...:
  5
  6
  7
  8
  9
  10
  11
  ```

---

## 36 - Useful Operators

```py
  In [1]: for num in range(10):  # default to start at zero
    ...:     print(num)
  0
  1
  2
  3
  4
  5
  6
  7
  8
  9
  
  In [2]: for num in range(3, 10):  # can also give a starting number
    ...:     print(num)
  3
  4
  5
  6
  7
  8
  9
  
  In [3]: for num in range(3, 10, 2):  # can also give a step size
    ...:     print(num)
  3
  5
  7
  9
  
  In [4]: list(range(0, 11, 2))  # can make them a list
  Out[4]: [0, 2, 4, 6, 8, 10]
  
  In [5]: index_count = 0
  
  In [6]: for letter in 'abcde':
    ...:     print('At index {} the letter is {}'.format(index_count, letter))  
    # the two variables in () tell what to put in each {}
    ...:     index_count += 1
  At index 0 the letter is a
  At index 1 the letter is b
  At index 2 the letter is c
  At index 3 the letter is d
  At index 4 the letter is e
  
  In [7]: index_count = 0
  
  In [8]: word = 'abcde'
  
  In [9]: for letter in word:
    ...:     print(word[index_count])
    ...:     index_count += 1
  a
  b
  c
  d
  e
  
  In [10]: word = 'abcde'
  
  In [11]: for i, l in enumerate(word):  
  # we don't have to include 'index', we can call enumerate
      ...:     print(i)
      ...:     print(l)
      ...:     print('\n')
  0
  a
  
  1
  b
  
  2
  c
  
  3
  d
  
  4
  e
  
  # enumerate takes in any iterable object and returns index counter and element at that iteration
  
  In [14]: mylist1 = [1, 2, 3, 4, 5]
  
  In [16]: mylist2 = ['a', 'b', 'c', 'd', 'e']
  
  In [17]: for item in zip(mylist1, mylist2):  # zip will zip the two lists together
      ...:     print(item)
  (1, 'a')
  (2, 'b')
  (3, 'c')
  (4, 'd')
  (5, 'e')
  
  In [18]: list(zip(mylist1, mylist2))  # again, we can put them in a list
  Out[18]: [(1, 'a'), (2, 'b'), (3, 'c'), (4, 'd'), (5, 'e')]
  
  In [19]: 'x' in [1, 2, 3]  # 'in' just asks if an element is in a list, dict, etc
  Out[19]: False
  
  In [20]: 'x' in ['a', 'b', 'x']
  Out[20]: True
  
  In [21]: 'mykey' in {'mykey':345}
  Out[21]: True
  
  In [22]: mylist = [10, 20, 30, 40, 100]
  
  In [23]: min(mylist)  # min
  Out[23]: 10
  
  In [24]: max(mylist)  # max
  Out[24]: 100
  
  In [25]: from random import shuffle  # imports shuffle from the random library
  
  In [26]: mylist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
  In [27]: shuffle(mylist)
  
  In [28]: mylist
  Out[28]: [9, 5, 1, 7, 6, 3, 10, 2, 8, 4]
  
  In [29]: from random import randint
  
  In [30]: randint(0, 1000000)
  Out[30]: 462184
  
  In [31]: randint(0, 1000000)
  Out[31]: 790231
  
  In [32]: mynum = randint(0, 99)
  
  In [33]: mynum
  Out[33]: 4
  
  In [34]: input('Enter a number: ')  # input is way to get information from the user
  
  Enter a number: 50
  Out[34]: '50'
  
  In [35]: result = input('enter your name: ')  # it is usually set to a variable
  
  enter your name: Darwin
  
  In [36]: result
  Out[36]: 'Darwin'
  
  In [37]: favnum = input('Enter your favorite number:')
  
  Enter your favorite number:42
  
  In [38]: favnum
  Out[38]: '42'                #  note the input is a string by default
  
  In [39]: favnum = int(input('Enter your favorite number:'))  
  # this will make it an integer
  
  Enter your favorite number:42
  
  In [40]: favnum
  Out[40]: 42
  ```

---

## 37 - List Comprehensions

### List Comprehensions are a unique way of quickly creating a list in Python

If you find yourself using a for loop along with .append() to create a list, List Comprehensions are a good alternative

```py
  In [1]: mystring = 'hello'
  
  In [2]: mylist = []
  
  In [3]: for letter in mystring:        # this would be a basic way of doing it
    ...:     mylist.append(letter)
  
  In [4]: mylist
  Out[4]: ['h', 'e', 'l', 'l', 'o']
  
  In [5]: mylist = [letter for letter in mystring]  # this is a list comprehension
  
  In [6]: mylist
  Out[6]: ['h', 'e', 'l', 'l', 'o']
  
  In [7]: mylist = [x for x in 'word']
  
  In [8]: mylist
  Out[8]: ['w', 'o', 'r', 'd']
  
  In [9]: mylist = [x for x in range(0,15)]  
  # we can also do other functions within the []
  
  In [10]: mylist
  Out[10]: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
  
  In [11]: mylist = [x**2 for x in range(0,15)]   # another example, squaring the values
  
  In [12]: mylist
  Out[12]: [0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196]
  
  In [13]: mylist = [x for x in range(0,15) if x%2==0]    # only if they are even
  
  In [14]: mylist
  Out[14]: [0, 2, 4, 6, 8, 10, 12, 14]
  
  In [15]: celcius = [0, 10, 20, 34.5]
  
  In [16]: fahrenheit = [((9/5)*temp + 32) for temp in celcius]
  # doing a C-F temp conversion
  
  In [17]: fahrenheit                                             # results
  Out[17]: [32.0, 50.0, 68.0, 94.1]
  
  In [18]: fahrenheit = []
  
  In [20]: for temp in celcius:
      ...:     fahrenheit.append(( (9/5)*temp + 32))   # the older way of doing it
  
  In [21]: fahrenheit
  Out[21]: [32.0, 50.0, 68.0, 94.1]
```

You can have if/else statements within the list comprehension, however this can get messy and unreadable and is not recommended

```py
  In [22]: results = [x if x%2==0 else 'ODD' for x in range(0,11)]
  # return x if x%2==0, else return 'ODD' for x in this range
  
  In [23]: results
  Out[23]: [0, 'ODD', 2, 'ODD', 4, 'ODD', 6, 'ODD', 8, 'ODD', 10]
  
  In [24]: mylist = []
  
  In [25]: for x in [2, 4, 6]:    # a nested loop
      ...:     for y in [100, 200, 300]:
      ...:         mylist.append(x*y)
  
  In [26]: mylist
  Out[26]: [200, 400, 600, 400, 800, 1200, 600, 1200, 1800]
  # each value of x is multiplied by each value of y (2 * 100, 2 * 200, etc... 6*300)
  
  In [34]: mylist = []
  
  In [35]: mylist = [x*y for x in [2, 4, 6] for y in [1, 10, 1000]]  
  # the list comprehension way of doing it.
  
  In [36]: mylist
  Out[36]: [2, 20, 2000, 4, 40, 4000, 6, 60, 6000]
  ```

---

## 38/39 - Python Statements Test

### Use for, .split(), and if to create a statement that will print out words that start with s:

```py
  In [6]: st = 'Print only the words that start with s in this sentence'
  
  In [7]: for i in st.split():  # splites the sentence at blanks
    ...:     if i[0] == "s":    # could also do i[0].lower to grab S and s
    ...:         print (i)
  start
  s
  sentence
```

### Use range() to print all even numbers from 0 to 10

```py
  In [8]: for n in range(11):
    ...:     if n%2==0:
    ...:         print(n)
  0
  2
  4
  6
  8
  10
  
  # Could also do:
  for n in range(0,11,2):
      print(num)
```

### Use a list comprehension to create a list of all numbers between 1 and 50 that are divisible by 3.

```py
  mylist = [x for x in range(1,51) if x%3==0]  # up to, but not including 51
  
  mylist
  Out[10]: [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48]
```

### Go through the string below and if the length of a word is even print "even!"

```py
  In [11]: st = 'Print every word in this sentence that has an even number of letters'
  
  In [12]: for i in st.split():
      ...:     if len(i) % 2 == 0:
      ...:         print(i)
  word
  in
  this
  sentence
  that
  an
  even
  number
  of
  
    # now to replace word with 'even!'
  
  In [1]: st = 'Print every word in this sentence that has an even number of letters'
  
  In [2]: for i in st.split():
    ...:     if len(i) % 2 == 0:
    ...:         print("even!")
    ...:
  even!
  even!
  even!
  even!
  even!
  even!
  even!
  even!
  even!
```

### FizzBuzz

Write a program that prints the integers from 1 to 100, but for multiples of three print "Fizz" instead of the number, and for multiples of five print "Buzz". For numbers that are multiples of both, print "FizzBuzz".

```py
  In [17]: for i in range(1, 101):
      ...:     if i%3==0 and i%5==0:
      ...:         print('FizzBuzz')
      ...:     elif i%3==0:
      ...:         print('Fizz')
      ...:     elif i%5==0:
      ...:         print('Buzz')
      ...:     else:
      ...:         print(i)
  1
  2
  Fizz
  4
  Buzz
  Fizz
  7
  8
  Fizz
  Buzz
  11
  Fizz
  13
  14
  FizzBuzz
  16
  .
  .
  .
  Fizz
  94
  Buzz
  Fizz
  97
  98
  Fizz
  Buzz
```

### Use a list comprehension to create a list of the first letters of every word in the string below:

```py
  In [18]: st = 'Create a list of the first letters of every word in this string'
  
  In [19]: newlist = [x[0] for x in st.split()]
  
  In [20]: newlist
  Out[20]: ['C', 'a', 'l', 'o', 't', 'f', 'l', 'o', 'e', 'w', 'i', 't', 's']
```

## Guessing Game

Let's use while loops to create a guessing game.

### The Challenge:

Write a program that picks a random integer from 1 to 100, and has players guess the number. The rules are:

1. If a player's guess is less than 1 or greater than 100, say "OUT OF BOUNDS"
2. On a player's first turn, if their guess is within 10 of the number, return "WARM!" If further than 10 away from the number, return "COLD!"
3. On all subsequent turns, if a guess is closer to the number than the previous guess return "WARMER!" If farther from the number than the previous guess, return "COLDER!"
4. When the player's guess equals the number, tell them they've guessed correctly and how many guesses it took!

```py
  In [18]: def guessme():
  import random
  num = random.randint(1, 100)
  print("WELCOME TO GUESS ME!")
  print("I'm thinking of a number between 1 and 100")
  print("If your guess is more than 10 away from my number, I'll tell you you're COLD")
  print("If your guess is within 10 of my number, I'll tell you you're WARM")
  print("If your guess is farther than your most recent guess, I'll say you're getting COLDER")
  print("If your guess is closer than your most recent guess, I'll say you're getting WARMER")
  print("LET'S PLAY!")
  
  guesses = [0]                # this keeps a list of guesses
  
  while True:
      guess = int(input("I'm thinking of a number between 1 and 100.\n What is your guess?"))
      if guess < 1 or guess > 100:
          print('Out of bounds! Please try again!: ')
          continue
      if guess == num:
          print(f'Congratulations! You guessed it in only {len(guesses)} guesses!!')  
          # len is 1 to start with
          break                      # if it is correct, we're done
      guesses.append(guess)
      # neither of the above was true, so we add that guess to the list
      if guesses[-2]:
      # if the guess before the last exists (i.e. this isn't the first guess)
          if abs(num - guess) < abs(num-guesses[-2]):    # if the new guess is better
              print('Warmer!')
          else:
              print('Colder!')
      else:                              # if there was no other guess, ....
          if abs(num - guess) <= 10:
              print('Warm!')
        else:
              print('Cold!')
  In  [19]: guessme()
  WELCOME TO GUESS ME!
  I'm thinking of a number between 1 and 100
  If your guess is more than 10 away from my number, I'll tell you you're COLD
  If your guess is within 10 of my number, I'll tell you you're WARM
  If your guess is farther than your most recent guess, I'll say you're getting COLDER
  If your guess is closer than your most recent guess, I'll say you're getting WARMER
  LET'S PLAY!
  
  I'm thinking of a number between 1 and 100.
  What is your guess?69
  Cold!
  
  I'm thinking of a number between 1 and 100.
  What is your guess?7
  Colder!
  
  I'm thinking of a number between 1 and 100.
  What is your guess?90
  Warmer!
  
  I'm thinking of a number between 1 and 100.
  What is your guess?80
  Colder!
  
  I'm thinking of a number between 1 and 100.
  What is your guess?95
  Warmer!
  
  I'm thinking of a number between 1 and 100.
  What is your guess?94
  Warmer!
  
  I'm thinking of a number between 1 and 100.
  What is your guess?92
  Colder!
  
  I'm thinking of a number between 1 and 100.
  What is your guess?93
  Congratulations! You guessed it in only 8 guesses!!
```

---
