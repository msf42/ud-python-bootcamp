# Section 06: Methods and Functions

## 40 - Methods and the Python Documentation

Built-in objects in Python have a variety of methods you can choose

```py
In [1]: mylist = [1, 2, 3]

In [2]: mylist.append(4)

In [3]: mylist
Out[3]: [1, 2, 3, 4]

In [4]: mylist.pop()
Out[4]: 4

In [5]: mylist
Out[5]: [1, 2, 3]

In [6]: help(mylist.insert)            ## methods: hit tab after . for list
Help on built-in function insert:      ## help(object.method) gives info

insert(...) method of builtins.list instance
    L.insert(index, object) -- insert object before index
```

- [docs.python.org](www.docs.python.org) has python documentation
- Stack Overflow is more readable

---

## 41 - Functions

Functions allow us to create blocks of code that can be easily executed many times, without needing to constantly rewrite the entire block of code

```py
def name_of_function():
    '''
    Docstring explains function
    '''
    print("Hello")

name_of_function()
Out: Hello

def name_of_function():
    '''
    Docstring explains function
    '''
    print("Hello " + name)

name_of_function("Steve")
Out: Hello Steve
```

Typically we use return keyword to return the output of a function instead of printing it

```py
def add_function(num1, num2):
    return num1 + num2

result = add_function(1, 2)

print(result)
Out: 3

In [1]: def name_function():
    ...:     print('Hello')

In [2]: name_function
Out[2]: <function __main__.name_function>   ## we need to provide input

In [3]: name_function()                     ## even if that input is ()
Hello

In [6]: def name_function2(): ## always use a docstring to explain the function
    ...:     '''
    ...:     DOCSTRING: Information about the function
    ...:     INPUT:
    ...:     OUTPUT: Hello
    ...:     '''
    ...:     print('Hello')

In [7]: help(name_function2)           ## this gives us the info and docstring
Help on function name_function2 in module __main__:

name_function2()
    DOCSTRING: Information about the function
    INPUT:
    OUTPUT: Hello

In [8]: def say_hello():
    ...:     print('hello')

In [10]: say_hello()
hello

In [11]: def say_hello(name):
    ...:     print('hello ' + name)

In [14]: say_hello('steve')
hello steve

In [15]: def say_hello(name = 'NAME'): ## this is a placeholder in case () is blank
    ...:     print('hello ' + name)

In [16]: say_hello()
hello NAME

In [17]: say_hello('Steve')
hello Steve

In [18]: def say_hello(name = 'NAME'):
    ...:     return 'hello ' + name    ## we use return instead of print

In [19]: result = say_hello("bitch")  ## so we can assign it to a variable

In [20]: result
Out[20]: 'hello bitch'

In [21]: def add(n1, n2):             ## take in 2 variables
    ...:     return n1 + n2

In [22]: result = add(20, 30)

In [23]: result
Out[23]: 50
```

## Find out if the word "dog" is in a string?

```py
In [31]: def dog_check(s):
    ...:     if 'dog' in s.lower():
    ...:         return True
    ...:     else:
    ...:         return False

In [32]: dog_check('Dog went away')
Out[32]: True

In [33]: def dog_check(s):
    ...:     return 'dog' in s.lower()

## we don't need the if statement, because this will return True/False

In [34]: dog_check('Dog went away')
Out[34]: True

##  Pig Latin
In [41]: def pig_latin(word):
    ...:     first_letter = word[0]
    ...:     if first_letter in 'aeiou':
    ...:         pig_word = word + 'ay'
    ...:     else:
    ...:         pig_word = word[1:] + first_letter + 'ay'
    ...:     return pig_word

In [42]: pig_latin('word')
Out[42]: 'ordway'

In [43]: pig_latin('apple')
Out[43]: 'appleay'

##  Prime numbers
In [47]: def is_prime(num):
    ...:     '''
    ...:     Naive method of checking for primes.
    ...:     '''
    ...:     for n in range(2, num):
    ...:         if num % n == 0:
    ...:             print(num, 'is not prime')
    ...:             break
    ...:     else: ## if never mod zero, then prime
    ...:         print(num, 'is prime!')

In [48]: is_prime(16)
16 is not prime

In [52]: is_prime(7)
7 is prime!

##  A better way
In [53]: import math

In [54]: def is_prime2(num):
    ...:     '''
    ...:     Better method for checking for primes.
    ...:     '''
    ...:     if num % 2 == 0 and num > 2:  ## getting rid of evens
    ...:         return False
    ...:     for i in range(3, int(math.sqrt(num)) + 1, 2):  
            ## no need to go beyond sqrt. only checks odds
    ...:         if num % i == 0:
    ...:             return False
    ...:     return True

In [55]: is_prime2(18)
Out[55]: False

In [59]: is_prime2(23)
Out[59]: True
```

---

## 42 - Overview of Quick Function Exercises #1-10

### Exercise 01

Write a function that prints the string "Hello World"

```py
def myfunc():
    print("Hello World")
```

### Exercise 02

Write a function that takes a name and prints "Hello Name"

```py
def myfunc(name):
    print("Hello {}".format(name))
```

### Exercise 03

Write a function that takes a Boolean value. If true, return "Hello", and if false, return "Goodbye"

```py
def myfunc(a):
    if a == True:
        return "hello"
    elif a == False:
        return "Goodbye"
```

### Exercise 04

Write a function that takes three arguments (x, y, and z). If z is True, return x. If z is false, return y.

```py
def myfunc(x, y, z):
    if z == True:
        return x
    else:
        return y
```

### Exercise 05

Write a function that takes two arguments and returns their sum.

```py
def myfunc(a, b):
    return a + b
```

### Exercise 06

Write a function that takes in one argument and returns True if the value is even, False if it is not

```py
def is_even(a):
    return (a % 2 == 0)
```

### Exercise 07

Write a function that takes two arguments and returns True if the first value is greater and False if it is less than or equal to the second.

```py
def is_greater(a, b):
    return a > b
```

### Exercise 08

Define a function that takes an arbitrary number of arguments, and returns the sum

```py
def myfunc(args):
    return sum(args)
```

### Exercise 09

Define a function that takes an arbitrary number of arguments, and returns a list of only the arguments that are even

```py
def myfunc(*args):
    mylist = []
    for item in args:
        if item % 2 == 0:
            mylist.append(item)
    return mylist
```

### Exercise 10

Write a function that takes in a string and returns a matching string where every even letter is uppercase, and every odd letter is lowercase.

```py
def myfunc(s):
    ret = ""                     ## empty return
    i = True                     ## i starts out True
    for char in s:
        if i:                    ## i starts as true, so first character will do this
            ret += char.upper()  ## make it upper case
        else:
            ret += char.lower()
        if char != ' ':          ## if it is NOT a space, then...
            i = not i            ## change i to false for next character
    return ret
```

---

## 43 - `*args` and `*kwargs`

### *args

```py
In [3]: def myfunc(a, b):
    ...:     ## Returns 5% of the sum of a and b
    ...:     return sum((a, b)) * 0.05

In [4]: myfunc(40, 60)
Out[4]: 5.0

In [5]: def myfunc(a, b, c=0, d=0, e=0):
    ...:     ## This allows us to accept 2-5 arguments
    ...:     ## Returns 5% of the sum of a and b (and c, d, e)
    ...:     return sum((a, b, c, d, e)) * 0.05

In [6]: myfunc(40, 60)
Out[6]: 5.0

In [7]: myfunc(40, 60, 50)
Out[7]: 7.5

In [8]: def myfunc(*args):          ## *args allows us to accept any number of items
    ...:     return sum(args) * 0.05

In [9]: myfunc(40, 60, 50)
Out[9]: 7.5

In [10]: myfunc(40, 60, 50, 23, 23, 67, 56, 67, 999)
Out[10]: 69.25

In [11]: def myfunc(*args):
    ...:     for item in args:
    ...:         print(item)

In [12]: myfunc(40, 60, 50, 23, 23, 67, 56, 67, 999)
40
60
50
23
23
67
56
67
999
```

### The "*args" allows us to accept any number of arguments.

These will be stored as a tuple
By convention, “args” is used, but any word can be used.
**kwargs are similar, but represent “keyword arguments”
They are stored as a dictionary instead of a tuple

### **kwargs

```py
In [15]: def myfunc(**kwargs):
    ...:     if 'fruit' in kwargs:
    ...:         print('My fruit of choice is {}'.format(kwargs['fruit']))
    ...:     else:
    ...:         print("I did not find any fruit here")

In [16]: myfunc(fruit='apple')
My fruit of choice is apple

In [17]: myfunc(fruit='apple', veggie = 'lettuce')
My fruit of choice is apple

In [18]: def myfunc(**kwargs):   ## To print it out and show the dictionary
    ...:     print(kwargs)
    ...:     if 'fruit' in kwargs:
    ...:         print('My fruit of choice is {}'.format(kwargs['fruit']))
    ...:     else:
    ...:         print("I did not find any fruit here")

In [19]: myfunc(fruit='apple', veggie = 'lettuce')
{'fruit': 'apple', 'veggie': 'lettuce'}
My fruit of choice is apple
### Using them in combination:
In [20]: def myfunc(*args, **kwargs):
    ...:  
    ...:     print('I would like {} {}'.format(args[0], kwargs['food']))

In [21]: myfunc(10, 20, 30, fruit = 'orange', food = 'eggs', animal = 'dog')
I would like 10 eggs
## Like we coded - take the first arg, then the kwarg that goes with 'food'

In [22]: def myfunc(*args, **kwargs):
    ...:     print(args)
    ...:     print(kwargs)
    ...:     print('I would like {} {}'.format(args[0], kwargs['food']))

In [23]: myfunc(10, 20, 30, fruit = 'orange', food = 'eggs', animal = 'dog')
(10, 20, 30)                             ## args are just a tuple
{'fruit': 'orange', 'food': 'eggs', 'animal': 'dog'} ## kwargs are a dictionary
I would like 10 eggs
```

---

## 44 - 48 - Function Practice Exercises

### Warm-up Section

Lesser of Two Evens: Write a function that returns the lesser of two given numbers if both numbers are even, but returns the greater if one or both numbers are odd

```py
def two_evens(a, b):
    if a % 2 == 0 and b % 2 == 0:
        if a > b:
            return b
        else:
            return a
    else:
        if a > b:
            return a
        else:
            return b

two_evens(6, 10)
Out[7]: 6

two_evens(7, 9)
Out[8]: 9

two_evens(6, 9)
Out[9]: 9
```

Write a function takes a two-word string and returns True if both words begin with same letter

```py
In [32]: def two_string(a):
    ...:     x = a.split()
    ...:     y = x[0]
    ...:     z = x[1]
    ...:     return y[0] == z[0]

In [33]: two_string("fuck you")
Out[33]: False

In [34]: two_string("fuck florida")
Out[34]: True
```

Given two integers, return True if the sum of the integers is 20 or if one of the integers is 20. If not, return False

```py
In [36]: def two_int(a, b):
    ...:     if a == 20 or b == 20:  ## if either is 20
    ...:         return True
    ...:     else:
    ...:         return a + b == 20  ## if they sum to 20, ret true, if not, ret false

In [37]: two_int(20, 30)
Out[37]: True

In [38]: two_int(8, 12)
Out[38]: True

In [39]: two_int(19, 2)
Out[39]: False
```

### Level 1 Problems

Write a function that capitalizes the first and fourth letters of a name**

```py
def first_fourth(a):
    x = a[:3]
    y = a[3:]
    z = x.capitalize() + y.capitalize()
    return z

first_fourth("fuaasdfasdfa")
Out[61]: 'FuaAsdfasdfa'
```

Given a sentence, return a sentence with the words reversed

```py
In [30]: def yoda(text):
    ...:     sent = text.split()
    ...:     newsent = []
    ...:     x = -1
    ...:     for i in sent:
    ...:         newsent.append(sent[x])
    ...:         x = x - 1
    ...:     return " ".join(newsent)

In [31]: yoda("this is a sentence")
Out[31]: 'sentence a is this'

In [32]: yoda("go fuck yourself")
Out[32]: 'yourself fuck go'
```

Given an integer n, return True if n is within 10 of either 100 or 200

```py
In [33]: def almost_there(n):
    ...:     return abs(n - 100) < 10 or abs(n - 200) < 10

In [34]: almost_there(195)
Out[34]: True

In [35]: almost_there(95)
Out[35]: True

In [36]: almost_there(109)
Out[36]: True

In [37]: almost_there(19)
Out[37]: False

In [38]: almost_there(222)
Out[38]: False
```

### Level 2 Problems

Given a list of ints, return True if the array contains a 3 next to a 3 somewhere.

```py
In [50]: def has_33(*arg):
    ...:     x = 1
    ...:     for i in arg:
    ...:         if arg[x] == arg[x + 1] == 3:
    ...:             return True
    ...:         else:
    ...:             x = x + 1
    ...:         return False

In [51]: has_33(1, 3, 4, 5, 6)
Out[51]: False

In [52]: has_33(1, 3, 3, 5, 6)
Out[52]: True
```

Given a string, return a string where for every character in the original there are three characters

```py
In [59]: def paperdoll(text):
    ...:     new = []
    ...:     for i in text:
    ...:         new.append(i[0])
    ...:         new.append(i[0])
    ...:         new.append(i[0])
    ...:     return "".join(new)

In [60]: paperdoll("steve")
Out[60]: 'sssttteeevvveee'

In [61]: paperdoll("fuck")
Out[61]: 'fffuuuccckkk'
```

### Blackjack

Given three integers between 1 and 11, if their sum is less than or equal to 21, return their sum. If their sum exceeds 21 and there's an eleven, reduce the total sum by 10. Finally, if the sum (even after adjustment) exceeds 21, return 'BUST'

```py
In [5]: def blackjack(a, b, c):
    ...:     sum = a + b + c
    ...:     newsum = sum - 10
    ...:     if sum < 22:
    ...:         return sum
    ...:     elif a == 11 or b == 11 or c == 11:
    ...:         return newsum
    ...:     else:
    ...:         return 'BUST'

In [6]: blackjack(2, 6, 8)
Out[6]: 16

In [7]: blackjack(2, 11, 10)
Out[7]: 13

In [8]: blackjack(9, 8, 6)
Out[8]: 'BUST'
```

Return the sum of the numbers in the array, except ignore sections of numbers starting with a 6 and extending to the next 9 (every 6 will be followed by at least one 9). Return 0 for no numbers.

```py
## Had to get help on this one, but now I understand it
In [1]:
    ...: def summer_69(arr):
    ...:     total = 0                 ## sum is 0 to begin
    ...:     add = True                ## add starts as True
    ...:     for num in arr:
    ...:         while add:
    ...:             if num != 6:      ## if the number is NOT 6, then add it
    ...:                 total += num  
    ...:                 break         ## break and add again
    ...:             else:             ## if number is 6, stop adding
    ...:                 add = False   ## now add becomes False
    ...:         while not add:        ## after the 6, don't add, but....
    ...:             if num != 9:      ## as long as the number is NOT 9, keep NOT adding
    ...:                 break
    ...:             else:             ## if you reach 9, add becomes True
    ...:                 add = True
    ...:                 break
    ...:     return total
    ...:
    ...:

In [2]: summer_69([1, 3, 5])
Out[2]: 9

In [3]: summer_69([4, 5, 6, 7, 8, 9])
Out[3]: 9

In [4]: summer_69([2, 1, 6, 9, 11])
Out[4]: 14
```

### Challenging Problems

#### SPY GAME:

Write a function that takes in a list of integers and returns True if it contains 007 in order

```py
spy_game([1,2,4,0,0,7,5]) --> True
spy_game([1,0,2,4,0,5,7]) --> True
spy_game([1,7,2,0,4,5,0]) --> False
## had to get help
def spy_game(nums):

    code = [0,0,7,'x']

    for num in nums:        ## for each number....
        if num == code[0]:  ## does it equal the first digit in 'code'?
            code.pop(0)
            ## if it does, remove the first digit of 'code' and keep searching
            ## code.remove(num) also works.

    return len(code) == 1  ## when the length is only 'x', 007 has been found

spy_game([1,2,4,0,0,7,5])
Out[2]: True

spy_game([1,0,2,4,0,5,7])
Out[3]: True

spy_game([1,7,2,0,4,5,0])
Out[4]: False
```

#### COUNT PRIMES:

Write a function that returns the number of prime numbers that exist up to and including a given number**

```py
## needed help with this one too
def count_primes(num):
    primes = [2]
    x = 3
    if num < 2:  ## for the case of num = 0 or 1
        return 0
    while x <= num:
        for y in range(3,x,2):  ## test all odd factors up to num
            if x%y == 0:        ## if x is evenly divisible by y
                x += 2          ## then it isn’t prime, so add 2 and start again
                break
        Else:                   ## if all are tested, it is prime
            primes.append(x)    ## so add it to the list
            x += 2              ## then add 2 and continue checking
    print(primes)
    return len(primes)


count_primes(100)
[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
Out[19]: 25
```

---

## 49 - Lambda Expressions, Map, and Filter Functions

### First, Maps

```py
In [1]: def square(num):
    ...:     return num**2
    ...:
    ...:

In [2]: my_nums = [1, 2, 3, 4, 5]

In [4]: for item in map(square, my_nums):  ## peform square func on all my_nums
    ...:     print(item)
    ...:
1
4
9
16
25

In [5]: list(map(square, my_nums))  ## put them in a list
Out[5]: [1, 4, 9, 16, 25]

In [6]: def splicer(mystring):
    ...:     if len(mystring) % 2 == 0:
    ...:         return 'EVEN'
    ...:     else:
    ...:         return mystring[0]
    ...:

In [7]: names = ['andy', 'eve', 'sally']

In [8]: list(map(splicer, names))  
## here we are not calling splicer to execute, we are just passing the function as an argument
Out[8]: ['EVEN', 'e', 's']
### Now Filters

Filters must us a function that returns True or False
Whereas map applies a function to every item in a list, filter only returns those items that pass the conditions of the function

In [9]: def check_even(num):
    ...:     return num % 2 == 0
    ...:
    ...:

In [10]: mynums = [1, 2, 3, 4, 5, 6]

In [11]: list(filter(check_even, mynums))  ## only returns mynums that pass check_even
Out[11]: [2, 4, 6]

In [12]: for n in filter(check_even, mynums):
    ...:     print(n)
    ...:
2
4
6
```

### Lambda Expressions

Lambda Expressions are one-time use functions

First, a regular function

```py
In [13]: def square(num):
    ...:     result = num ** 2
    ...:     return result
    ...:
    ...:

In [14]: square(9)
Out[14]: 81
```

Now, changing that to a lambda function

```py
In [13]: def square(num):        ## a regular function
    ...:     result = num ** 2
    ...:     return result
    ...:

In [14]: square(9)
Out[14]: 81

In [15]: def square(num):  ## we can shorten it
    ...:     return num ** 2
    ...:

In [16]: square(9)
Out[16]: 81

In [17]: def square(num): return num ** 2  ## shorten to a single line

In [18]: square(9)
Out[18]: 81

In [19]: square = lambda num: num ** 2        ## lambda input: output
## and make it a lambda function, assigning it to square

In [20]: square(9)
Out[20]: 81
```

### lambda functions are sometimes called ***anonymous functions***, meaning we don't have to define them

Here we can map a lambda function

```py
In [21]: list(map(lambda num: num ** 2, mynums))  
Out[21]: [1, 4, 9, 16, 25, 36]

##  or filter one
In [22]: list(filter(lambda num: num % 2 == 0, mynums))
Out[22]: [2, 4, 6]

In [24]: names = ['andy', 'eve', 'sally']

In [27]: list(map(lambda x:x[0], names))  
## here we map the lambda function to the names list, giving the first letter of each item
Out[27]: ['a', 'e', 's']

In [28]: list(map(lambda x:x[::-1], names))  ## here we reverse the names
Out[28]: ['ydna', 'eve', 'yllas']

---

## 50 - Nested Statements and Scope

```py
In [1]: x = 25

In [2]: def printer():
    ...:     x = 50
    ...:     return x
    ...:

In [3]: print(x)
25

In [5]: print(printer())
50
```

Note the different results above in relation to the value of ‘x’

### LEGB Rule

- **L = Local** - Names assigned in any way within a function (def or lambda) and not declared global in that function
- **E = Enclosing function locals** - Names in the local scope of any and all enclosing functions (def or lambda), from inner to outer
- **G = Global (module)** - Names assigned at the top-level of a module file, or declared global in a def within the file
- **B = Built-in (Python)** - Names preassigned in the built-in names module: open, range, SyntaxError, …

```py
In [6]: name = 'This is a global string'  #GLOBAL

In [7]: def greet():
    ...:     name = 'Sammy'  #ENCLOSING
    ...:     def hello():
                ## IF WE ADDED A LINE HERE, IT WOULD BE LOCAL
    ...:         print('Hello '+name)  
    ## where is the nearest defined name? It is in the enclosing function
    ...:     hello()
    ...: greet()
    ...:
    ...:
Hello Sammy
```

## If we commented out name='Sammy', it would print "Hello This is a global string"

When you assign within a function, that variable's *scope* will only apply within that function

```py
In [8]: x = 50

In [10]: def func():
    ...:     print(f'X is {x}')
    ...:     ## Local Reassignment
    ...:     x = 200
    ...:     print(f'I just localy changed x to {x}')
    ...:

In [11]: func(x)
X is 50
I just localy changed x to 200

In [12]: print(x)
50

## Let's change the global value of x within the function
In [14]: def func():
    ...:     global x
    ...:     print(f'X is {x}')
    ...:     ## Local Reassignment
    ...:     x = 300
    ...:     print(f'I just localy changed GLOBAL x to {x}')
    ...:
    ...:

In [15]: func()
X is 50
I just localy changed GLOBAL x to 300

In [16]: print(x)
300
```

## 51/52/53 - Functions and Methods - Homework Assignment

### Write a function that computes the volume of a sphere given its radius

Hint: Write out your solution by hand as an equation with an input radius, then try to convert your logic to code.

```py
In [3]: def vol(rad):
    ...:     import math
    ...:     print((4/3)*math.pi*rad**3)
    ...:

In [4]: vol(3)
113.09733552923255
```

### Write a function that checks whether a number is in a given range (inclusive of high and low)

Hint: Keep in mind how to use the in operator and the range() function. How can you use in to check if something is in a list?

```py
In [13]: def ran_check(num, low, high):
    ...:     if num in range(low, high):
    ...:         return "Yes"
    ...:     else:
    ...:         return "No"
    ...:

In [14]: ran_check(3, 4, 10)
Out[14]: 'No'

In [15]: ran_check(44, 2, 1000)
Out[15]: 'Yes'
```

If you only wanted to return a boolean:

```py
In [6]: def ran_bool(num, low, high):
    ...:     return num>=low and num<=high
    ...:

In [7]: ran_bool(2, 4, 20)
Out[7]: False

In [8]: ran_bool(44, 2, 222)
Out[8]: True
```

### Write a function that accepts a string and calculate the number of upper case letters and lower case letters

Hint: What is a good way to iterate through the string? A for loop perhaps? How can you keep track of you counts? A variable or maybe even a dictionary with a key for upper and a key for lower, then add counts to the value corresponding to the correct key.

```py
In [17]: def uplow(s):
    ...:     import string
    ...:     low = string.ascii_lowercase
    ...:     up = string.ascii_uppercase
    ...:     lowtotal = 0
    ...:     uptotal = 0
    ...:     for i in s:
    ...:         if i in low:
    ...:             lowtotal += 1
    ...:         else:
    ...:             continue
    ...:     for i in s:
    ...:         if i in up:
    ...:             uptotal += 1
    ...:         else:
    ...:             continue
    ...:     print(f'Total lower case = {lowtotal}')
    ...:     print(f'Total upper case = {uptotal}')
    ...:

In [18]: uplow("Welcome to the Jungle")
Total lower case = 16
Total upper case = 2
```

### Write a function that takes a list and returns a new list with the unique elements of the first list

Hint: Try using set() or even just using a for loop with a list and append numbers that you haven't already seen

```py
In [5]: def unique_list(l):
    ...:     return set(l)
    ...:

In [6]: unique_list([3, 3, 4, 5, 6, 6, 6, 6, 7])
Out[6]: {3, 4, 5, 6, 7}
```

### Write a function to multiply all of the numbers in a list

Hint: Use a for loop to go through the list and take advantage of *= which operate similarly to +=.

```py
In [6]: def multiply(numbers):
    ...:     total = 1
    ...:     for i in numbers:
    ...:         total *= i
    ...:     return total
    ...:
    ...:

In [7]: multiply([10, 10, 10])
Out[7]: 1000

In [8]: multiply([3, 4, 5, 6, 7, 2, 8, 9, 10])
Out[8]: 3628800
```

### Write a function that checks whether a passed string is a palindrome or not

Hint: Remember what s[::-1] does if s is a string, or find a built-in way to reverse a string (function or method)

```py
In [15]: def palindrome(s):
    ...:     reverse = s[::-1]
    ...:     return s == reverse
    ...:

In [16]: palindrome("steve")
Out[16]: False

In [17]: palindrome("racecar")
Out[17]: True

In [18]: palindrome("tacocat")
Out[18]: True
```

### Write a function that will check whether a string is a pangram or not

A pangram is a word or sentence that contains every letter at least once
Hint: This is really tricky, but try using set() and feel free to check the answer for this one!

```py
In [59]: import string

In [60]: def ispangram(str1, alphabet=string.ascii_lowercase):
    ...:     str2 = str1.lower()
    ...:     letters = set(str2)
    ...:     for i in alphabet:
    ...:         if i not in letters:
    ...:             return False
    ...:         else:
    ...:             continue
    ...:     return True
    ...:

In [61]: ispangram("The quick brown fox jumps over The lazy dog")
Out[61]: True

In [62]: ispangram("abcdefghijklmnopqrstuvwxyz")
Out[62]: True

In [63]: ispangram("abcdefghijklmnopqrstuvwxy")
Out[63]: False

In [64]: ispangram("abcdefghijklmnopqrstuvwxyZ")
Out[64]: True
```

---
