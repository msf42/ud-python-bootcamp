# Section 03: Python Objects & Data Structure Basics

---

## 10 - Intro to Python Data Types

These are basic building blocks of code when constructing larger pieces of code

### Data Types:

#### Integers (int)

- Whole Numbers
- 3 300 2000

#### Floating Point (float)

- Numbers with a decimal point
- 2.3 4.7 100.0

#### Strings(str)

- Ordered sequence of characters
- “Hello” ‘Sammy’ ‘2000’

#### Lists (list)

- Ordered sequence of objects
- [10, “hello”, 200.3]

#### Dictionaries (dict)

- Unordered key:value pairs
- {“mykey”:”value”, “name”:”Frank”}

#### Tuples (tup)

- Ordered immutable sequence of objects
- (10, “hello”, 200.3)

#### Sets (set)

- Unordered collection of unique objects
- {“a”, “b”}

#### Booleans (bool)

- Logical value indicating True or False
- 2 < 3
- 2 > 3

---

## 11 - Numbers

There are two main number types

- Integers
- Floating Point Numbers

```py
  In [1]: 2 - 1 # Simple subtraction
  Out[1]: 1

  In [2]: 2 + 1 # Simple addition
  Out[2]: 3

  In [3]: 2 * 2 # Simple multiplication
  Out[3]: 4
```

---

## 12 - Numbers FAQ

### Q: What's the difference between floating point and an integer?

A: An integer has no decimals in it, a floating point number can display digits past the decimal point.

### Q: Why doesn't 0.1+0.2-0.3 equal 0.0 ?

A: This has to do with floating point accuracy and computer's abilities to represent numbers in memory.

For a full breakdown, check out [this tutorial](https://docs.python.org/2/tutorial/floatingpoint.html)

---

## 13 - Variable Assignments

### Name Rules

- No spaces; use underscore
- Can not start with a number
- Best to use lower case
- Avoid words that have meaning in Python, such as list, str, etc

### Dynamic Typing (as opposed to Static Typing)

You can reassign variable types
Examples:

```py
  In [1]: my_dogs = 2

  # ... later in code
  In [2]: my_dogs = ["Sammy", "Frankie"]

  In [1]: a = 10
  In [2]: a + a
  Out[2]: 20

  In [3]: a = a + a  #we have a new value for 'a'
  In [4]: a  
  Out[4]: 20

  In [5]: a = a + a  #'a' is reassigned again
  In [6]: a
  Out[6]: 40

  In [7]: type(a)  #this tells you the type of variable
  Out[7]: int

  In [8]: a = 30.1
  In [9]: type(a)
  Out[9]: float

  # simple calculations using variables
  In [10]: my_income = 100
  In [11]: tax_rate = 0.1
  In [12]: my_taxes = my_income * tax_rate

  In [13]: my_taxes  
  Out[13]: 10.0
```

---

## 14 - Intro to Strings

Strings are sequences of characters using either single or double quotes

- `“Hello”`
- `‘hello’`
- `“I don’t like that”` (note the single quote within the double quotes)

```py
  In [1]: 'hello'
  Out[1]: 'hello'

  In [2]: "world"
  Out[2]: 'world'

  In [3]: "I'm going on a run"
  Out[3]: "I'm going on a run"

  In [4]: print("hello")
  hello

  In [5]: print('hello \nworld') # /n = new line
  hello
  world

  In [6]: print('hello \tworld') # /t = tab
  hello   world

  In [7]: len('hello')           # returns length of string
  Out[7]: 5

  In [8]: len('fuck you')        # spaces count too
  Out[8]: 8
```

---

## 15 - Indexing and Slicing with Strings

Strings are ordered sequences, which means we can use indexing or slicing to grab sections

### Indexing

uses `[ ]` notation after the string name and the corresponding index number

| Character     | **H** | **e** | **l** | **l** | **o** |
| ------------- | ----- | ----- | ----- | ----- | ----- |
| Index         | 0     | 1     | 2     | 3     | 4     |
| Reverse Index | 0     | -4    | -3    | -2    | -1    |

### Slicing

uses `[start:stop:step]` syntax
**start** = numerical index for the slice start
**stop** = numerical index for the slice stop, but is NOT included
**step** = the size of the step you take

```py
  In [1]: mystring = "Hello World"

  In [2]: mystring[0]  # returns index 0
  Out[2]: 'H'

  In [3]: mystring[8]  # returns index 8
  Out[3]: 'r'

  In [4]: mystring[-2]  # returns index -2; last letter = -1
  Out[4]: 'l'

  In [5]: mystring  # the original string is unchanged
  Out[5]: 'Hello World'

  In [6]: mystring = 'abcdefghijk'  # now we reassign it

  In [7]: mystring[2]  # same as before
  Out[7]: 'c'

  In [8]: mystring[2:]  # this slices from 2 until the end
  Out[8]: 'cdefghijk'

  In [9]: mystring[:3]  # this slices from beginning up to, but not including, 3
  Out[9]: 'abc'

  In [10]: mystring  # the string is unchanged
  Out[10]: 'abcdefghijk'

  In [11]: mystring[3:6]  # slices from 3, up to, but not including, index 6
  Out[11]: 'def'

  In [12]: mystring[::]  # slices everything
  Out[12]: 'abcdefghijk'

  In [13]: mystring[::2]  # slices everything by 2s
  Out[13]: 'acegik'

  In [14]: mystring[::3]  # slices by 3s
  Out[14]: 'adgj'

  In [15]: mystring[2:7:2]  # starts at 2, ends at 7, by 2s
  Out[15]: 'ceg'

  In [16]: mystring[::-1]  # slices by -1s, so it reverses the string
  Out[16]: 'kjihgfedcba'
```

---

## 16 - String Properties and Methods

### Immutability

An index within a string can not be changed
However, Strings can be redefined

```py
  In [1]: name = "Sam"
```

## Strings are immutable

```py
  In [2]: name[0] = 'P' # this will give an error. We can't change an index

  TypeError: 'str' object does not support item assignment

  In [3]: last_letters = name[1:]
  In [4]: last_letters
  Out[4]: 'am'

  In [5]: 'P' + last_letters
  Out[5]: 'Pam'

  In [6]: x = 'Hello World'
  In [7]: x + ' it is beautiful outside!'
  Out[7]: 'Hello World it is beautiful outside!'

  In [8]: x  # x is unchanged
  Out[8]: 'Hello World'

  In [9]: letter = 'z'
  In [10]: letter * 10  # we can multiply strings
  Out[10]: 'zzzzzzzzzz'

  In [11]: 2 + 3
  Out[11]: 5

  In [12]: '2' + '3'  # since these are strings, they will be concatenated
  Out[12]: '23'

  In [13]: x.upper()
  Out[13]: 'HELLO WORLD'

  In [14]: x  # x is unchanged
  Out[14]: 'Hello World'

  In [15]: x.split()  # default is to split at white spaces
  Out[15]: ['Hello', 'World']

  In [16]: x.split('l')  # this will split at each 'l'
  Out[16]: ['He', '', 'o Wor', 'd']
```

---

## 17 - Strings FAQ

### Q. Are strings mutable?

A. Strings are not mutable! (meaning you can’t use indexing to change individual elements of a string)

### Q. How do I create comments in my code?

A. You can use the hashtag # to create comments

---

## 18 - Print Formatting with Strings

You may want to ‘inject’ a variable into a string for printing.
For example:

```py
  In [1]: my_name = "Jose"
  In [2]: print("Hello " + my_name)
  Hello Jose
```

This is known as **string interpolation**. There are two methods:

- .format() method
- F-strings (formatted string literals)

### The .format() method:

The syntax is:

```py
print('The {} {} {}'.format('fox', 'brown', 'quick'))
```

```py
In [1]: print('This is a string {}'.format('INSERTED'))  
# the parentheses tell us what to insert in brackets
This is a string INSERTED

In [2]: print('The {} {} {}'.format('fox', 'brown', 'quick'))  
# you can have multiple brackets
The fox brown quick

In [3]: print('The {2} {1} {0}'.format('fox', 'brown', 'quick'))  
# you can change the order with indexing
The quick brown fox

In [4]: print('The {0} {0} {0}'.format('fox', 'brown', 'quick'))  
# you can even repeat
The fox fox fox

In [5]: print('The {q} {b} {f}'.format(f = 'fox', b = 'brown', q = 'quick'))  
# you can also define them with keywords
The quick brown fox
```

#### Float formatting follows “{value:width.precision f}”

```py
In [1]: result = 100/777

In [2]: result
Out[2]: 0.1287001287001287

In [3]: print("The result was {}".format(result))
The result was 0.1287001287001287

In [4]: print("The result was {r}".format(r = result))  
# add in the 'r' to represent the result
The result was 0.1287001287001287

In [5]: print("The result was {r:1.3f}".format(r = result))  
# 1 space to the left, 3 floats
The result was 0.129

In [6]: print("The result was {r:10.3f}".format(r = result))  
# 10 spaces
The result was      0.129

In [7]: print("The result was {r:1.5f}".format(r = result))  
# 5 floats
The result was 0.12870

In [8]: result = 104.12345

In [9]: print("The result was {r:1.5f}".format(r = result))  
The result was 104.12345

In [10]: print("The result was {r:1.2f}".format(r = result))
The result was 104.12

### The f-strings method (introduced in Python 3.6)
In [1]: name = "Jose"
In [2]: print('Hello, his name is {}'.format(name))
Hello, his name is Jose

In [3]: print(f'Hello, his name is {name}')
Hello, his name is Jose

In [4]: name = "Sam"
In [5]: age = 3
In [6]: print(f'{name} is {age} years old.')
Sam is 3 years old.
```

---

## 19 - Print Formatting FAQ

### Q. I imported print from the future module, now print isn't working. What happened?

A. This is because once you import from the future module in Python 2.7, a print statement will no longer work, and print must then use a print() function. Meaning that you must use:

```py
print('Whatever you were going to print')
# or if you are using some formatting:
print('This is a string with an {p}'.format(p='insert'**))
```

The future module allows you to use Python3 functionality in a Python2 environment, but some functionality is overwritten (such as the print statement, or classic division when you import division).
Since we are using Jupyter Notebooks, once you so the import, all cells will require the use if the print() function. You will have to restart Python or start a new notebook to regain the old functionality back.
Here is an awesome source for print formatting: [link](https://pyformat.info)

---

## 20 - Lists in Python

```py
Lists are ordered sequences that can hold a variety of object types
They use [brackets] and commas to separate objects in the list
[1, 2, 3, 4, 5]
They can be nested, and have a variety of useful methods

In [1]: my_list = [1, 2, 3]
In [2]: my_list = ['string', 100, 23.2]
In [3]: len(my_list)
Out[3]: 3

In [4]: my_list = ['one', 'two', 'three']
In [5]: my_list[0]
Out[5]: 'one'

In [6]: my_list[1:]
Out[6]: ['two', 'three']

In [7]: my_list
Out[7]: ['one', 'two', 'three']

In [8]: another_list = ['four', 'five']
In [9]: my_list + another_list
Out[9]: ['one', 'two', 'three', 'four', 'five']

In [10]: my_list
Out[10]: ['one', 'two', 'three']

In [11]: new_list = my_list + another_list
In [12]: new_list
Out[12]: ['one', 'two', 'three', 'four', 'five']

In [13]: new_list[0] = 'ONE'
In [14]: new_list
Out[14]: ['ONE', 'two', 'three', 'four', 'five']

In [15]: new_list.append('six')
In [16]: new_list
Out[16]: ['ONE', 'two', 'three', 'four', 'five', 'six']

In [17]: new_list.append('seven')
In [18]: new_list
Out[18]: ['ONE', 'two', 'three', 'four', 'five', 'six', 'seven']

In [19]: new_list.pop()
Out[19]: 'seven'

In [20]: popped_item = new_list.pop()
In [21]: popped_item
Out[21]: 'six'

In [22]: new_list.pop(0)
Out[22]: 'ONE'

In [23]: new_list
Out[23]: ['two', 'three', 'four', 'five']

In [24]: new_list = ['a', 'e', 'x', 'b', 'c']
In [25]: num_list = [4, 1, 8, 3]
In [26]: new_list.sort()
In [27]: new_list
Out[27]: ['a', 'b', 'c', 'e', 'x']

In [28]: num_list.sort()
In [29]: num_list
Out[29]: [1, 3, 4, 8]

In [30]: num_list.reverse()
In [31]: num_list
Out[31]: [8, 4, 3, 1]
```

---

## 21 - Lists FAQ

### Q. How do I index a nested list? For example if I want to grab 2 from [1,1,[1,2]]?

A. You would just add another set of brackets for indexing the nested list, for example: `my_list[2][1]` . We'll discover later on more nested objects and you will be quizzed on them later!

---

## 22 - Dictionaries in Python

### Dictionaries are unordered mappings for storing objects

Unlike lists, there is no order
 Instead, there is a key:value pair that allows users to grab objects without needing to know an index location

### Dictionaries use curly braces and colons

```py
{‘key1’:’value1’, ‘key2’:’value2’}
```

### Lists vs Dictionaries

Dictionaries

- objects are retrieved by name
- Unordered and can not be sorted

Lists

- objects retrieved by location
- Allows slicing, sorting, and indexing
- Trade off: Speed for sorting, etc

```py
In [1]: my_dict = {'key1':'value1', 'key2':'value2'}

In [2]: my_dict
Out[2]: {'key1': 'value1', 'key2': 'value2'}

In [3]: my_dict['key1']  # Give the key, get the value
Out[3]: 'value1'

In [6]: prices_lookup = {'apple':2.99, 'oranges':1.99, 'milk':5.80}

In [7]: prices_lookup['apple']  # This could be helpful for very large dictionaries
Out[7]: 2.99

In [8]: d = {'k1':123, 'k2':[0, 1, 2], 'k3':{'insideKey':100}}  # A value can be a list, another dictionary, etc

In [11]: d['k2']
Out[11]: [0, 1, 2]

In [12]: d['k3']
Out[12]: {'insideKey': 100}

In \[13]: d['k3'\]['insideKey']
Out[13]: 100

In \[14]: d['k2'\][2]
Out[14]: 2

In [16]: d = {'key1':['a', 'b', 'c']}

In [17]: d
Out[17]: {'key1': ['a', 'b', 'c']}

In [18]: mylist = d['key1']  # We can assign variables to these values

In [19]: letter = mylist[2]

In [20]: letter
Out[20]: 'c'

In [21]: letter.upper()
Out[21]: 'C'

In \[23]: d['key1'\][2].upper()  # This is a shortcut way of doing lines 19-21
Out[23]: 'C'

In [24]: d = {'k1':100, 'k2':200}

In [25]: d['k3'] = 300  # We can add new entries

In [26]: d
Out[26]: {'k1': 100, 'k2': 200, 'k3': 300}

In [27]: d['k1'] = 999  # We can reassign them

In [28]: d
Out[28]: {'k1': 999, 'k2': 200, 'k3': 300}

In [29]: d.keys()  # List all of the keys
Out[29]: dict_keys(['k1', 'k2', 'k3'])

In [30]: d.values()  # List all of the values
Out[30]: dict_values([999, 200, 300])

In [31]: d.items()  # List all of the items as a tuple
Out[31]: dict_items([('k1', 999), ('k2', 200), ('k3', 300)])  # This is a tuple
```

---

## 23 - Dictionaries FAQ

### Q. Do dictionaries keep an order? How do I print the values of the dictionary in order?

A. Dictionaries are mappings and do not retain order! If you do want the capabilities of a dictionary but you would like ordering as well, check out the `**ordereddict**` object lecture later on in the course!

---

## 24 - Tuples with Python

Tuples

- are simply immutable lists
- Elements can not be reassigned
- While lists use [ ], tuples use ()

```py
In [1]: t = (1, 2, 3)  # a tuple

In [2]: mylist = [1, 2, 3]  # a list

In [3]: type(t)
Out[3]: tuple

In [4]: type(mylist)
Out[4]: list

In [5]: len(t)
Out[5]: 3

In [6]: len(mylist)
Out[6]: 3

In [7]: t = ('one', 2)  # just as flexible as lists

In [8]: t[0]  # can be indexed like lists
Out[8]: 'one'

In [9]: t = ('a', 'a', 'b')

In [10]: t.count('a')  # counts the appearances of 'a'
Out[10]: 2

In [11]: t.index('a')  # returns the FIRST appearance of 'a'
Out[11]: 0

In [12]: t.index('b')
Out[12]: 2

In [13]: t
Out[13]: ('a', 'a', 'b')

In [14]: mylist
Out[14]: [1, 2, 3]

In [15]: mylist[0] = 'new'  # in lists, we can re-assign items

In [16]: mylist
Out[16]: ['new', 2, 3]

In [17]: t[0] = 'new'  # reassignment not possible with tuples. It will return an error
TypeError: 'tuple' object does not support item assignment
```

---

## 25 - Sets in Python

Sets

- unordered collections of unique elements
- there can only be one representative of the same object

```py
In [1]: myset = set()  # note the word set is before ( )

In [2]: myset
Out[2]: set()

In [3]: myset.add(1)

In [4]: myset
Out[4]: {1}

In [5]: myset.add(2)

In [6]: myset
Out[6]: {1, 2}

In [7]: myset.add(2)

In [8]: myset  # Note the second 2 does not get added
Out[8]: {1, 2}

In [9]: mylist = [1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4]

In [10]: set(mylist)  # this only returns each item once
Out[10]: {1, 2, 3, 4}
```

---

## 26 - Booleans in Python

Booleans

- are operators that allow you to convey either True or False statements
- Very important later when dealing with control flow and logic
- Must be capitalized

```py
In [12]: type(False)
Out[12]: bool

In [13]: 1 > 2
Out[13]: False

In [14]: 1 == 1
Out[14]: True

In [15]: b = None  # None is a placeholder so that we can assign the type later

In [16]: type(b)
Out[16]: NoneType
```

---

## 27 - I/O Basic Files in Python

```py
In [5]: import os

In [6]: os.chdir("/home/steve/Dropbox/Steve_Python/Udemy Bootcamp")  # change directory

In [7]: pwd  # print working directory
Out[7]: '/home/steve/Dropbox/Steve_Python/Udemy Bootcamp'

In [8]: %%writefile myfile.txt  # ipython magic for writing a file
  ...: Hello this is a text file
  ...: this is the second line
  ...: this is the third line
  ...:
Writing myfile.txt

In [9]: myfile = open('myfile.txt')

In [10]: myfile = open('oops.txt')  # returns an error
---------------------------------------------------------------------------
#FileNotFoundError                         Traceback (most recent call last)
#<ipython-input-10-1db77fdcf6f5> in <module>()
#----> 1 myfile = open('oops.txt')

#FileNotFoundError: [Errno 2] No such file or directory: 'oops.txt'

In [11]: pwd
Out[11]: '/home/steve/Dropbox/Steve_Python/Udemy Bootcamp'

In [12]: myfile.read()  # reads the file
Out[12]: 'Hello this is a text file\nthis is the second line\nthis is the third line\n'

In [13]: myfile.read()  # the file is already read, so nothing returned
Out[13]: ''

In [14]: myfile.seek(0)  # resets to the beginning of the file
Out[14]: 0

In [15]: myfile.read()
Out[15]: 'Hello this is a text file\nthis is the second line\nthis is the third line\n'

In [16]: myfile.seek(0)
Out[16]: 0

In [17]: contents = myfile.read()

In [18]: contents
Out[18]: 'Hello this is a text file\nthis is the second line\nthis is the third line\n'

In [19]: myfile.seek(0)
Out[19]: 0

In [20]: myfile.readlines()  # separates the lines
Out[20]:
['Hello this is a text file\n',
'this is the second line\n',
'this is the third line\n']

In [21]: myfile.close()

In [22]: with open('myfile.txt') as my_new_file:  # reads the contents without having to worry about closing it
  ....:     contents = my_new_file.read()
  ....:

In [23]: contents
Out[23]: 'Hello this is a text file\nthis is the second line\nthis is the third line\n'

In [24]: with open('myfile.txt', mode='r') as myfile:  # see below
  ....:     contents = myfile.read()


In [25]: %%writefile my_new_file.txt
  ....: ONE ON FIRST
  ....: TWO ON SECOND
  ....: THREE ON THIRD
  ....:
Writing my_new_file.txt

In [26]: %%writefile my_new_file.txt
ONE ON FIRST
TWO ON SECOND
THREE ON THIRDDDD
  ....:
Overwriting my_new_file.txt  # This will overwrite

In [28]: with open('my_new_file.txt', mode='r') as f:
  ....:     print(f.read())
  ....:
ONE ON FIRST
TWO ON SECOND
THREE ON THIRDDDD

In [29]: with open('my_new_file.txt', mode='a') as f:  # Appending
  f.write('FOUR ON FOURTH')
  ....:

In [30]: with open('my_new_file.txt', mode='r') as f:
  print(f.read())
  ....:
ONE ON FIRST
TWO ON SECOND
THREE ON THIRDDDD
FOUR ON FOURTH

In [31]: with open('my_new_file2.txt', mode='w') as f:  # a new file
  f.write('I created this file')
  ....:

In [32]: with open('my_new_file2.txt', mode='r') as f:
  print(f.read())
  ....:
I created this file
```

---

## 28 - Resources for More Basic Practice

Before you begin your assessment, I wanted to point out some helpful links for practice (don't worry about being able to do these exercises, I just want you to be aware of the links so you can visit them later, since we still haven't discussed functions, you won't be able to utilize a lot of these resources yet!):
[Basic Practice](http://codingbat.com/python)
[More Mathematical (and Harder) Practice](https://projecteuler.net/archives)
[List of Practice Problems](http://www.codeabbey.com/index/task_list)
[A SubReddit Devoted to Daily Practice Problems](https://www.reddit.com/r/dailyprogrammer)
[very tricky website with very few hints and tough problems (Not for beginners but still interesting)](http://www.pythonchallenge.com/)

---

## 29/30 - Python Objects and Data Structures Assessment Test

### Testing Your Knowledge: Answer the following questions:

Write a brief description of all the following Object Types and Data Structures we've learned about:

- Numbers: integers, floats, etc
- Strings: Strings of characters
- Lists: Made with [ ]. Can be indexed or sliced. Can be changed.
- Tuples: Like a list, but immutable.
- Dictionaries: Unordered. Consists of keys and values.

### Numbers

Write an equation that uses multiplication, division, an exponent, addition, and subtraction that is equal to 100.25.
Hint: This is just to test your memory of the basic arithmetic commands, work backwards from 100.25

```py
  In [3]: 2 * (10 - 5) + 902.5/10
  Out[3]: 100.25
```

Answer these 3 questions without typing code. Then type code to check your answer.

- What is the value of the expression 4 * (6 + 5) = 44
- What is the value of the expression 4 * 6 + 5 = 29
- What is the value of the expression 4 + 6 * 5 = 34

```py
  In [1]: 4 * (6 + 5)
  Out[1]: 44

  In [2]: 4 * 6 + 5
  Out[2]: 29

  In [3]: 4 + 6 * 5
  Out[3]: 34
```

What is the ***type** of the result of the expression 3 + 1.5 + 4?
What would you use to find a number’s square root, as well as its square?

```py
  In [4]: type(3 + 1.5 + 4)
  Out[4]: float

  In [5]: 9**(1/2)
  Out[5]: 3.0

  In [6]: 3**2
  Out[6]: 9
```

### Strings

Given the string 'hello' give an index command that returns 'e'. Enter your code in the cell below:

```py
s = 'hello'
# Print out 'e' using indexing
In [8]: s[1]
Out[8]: 'e'
# Reverse the string using slicing
In [10]: s[::-1]
Out[10]: 'olleh'
```

Given the string hello, give two methods of producing the letter 'o' using indexing.

```py
s ='hello'
# Print out the 'o'

# Method 1:
In [11]: s[-1]
Out[11]: 'o'

# Method 2:
In [12]: s[4]
Out[12]: 'o'
```

### Lists

Build this list [0,0,0] two separate ways.

```py
# Method 1:
In [13]: l = [0, 0, 0]

In [14]: l
Out[14]: [0, 0, 0]

# Method 2:
In [15]: newlist = []

In [16]: newlist.append(0)

In [17]: newlist.append(0)

In [18]: newlist.append(0)

In [19]: newlist
Out[19]: [0, 0, 0]
```

Reassign 'hello' in this nested list to say 'goodbye' instead:

```py
In [28]: list3 = [1, 2, [3, 4, 'hello']]
In [30]: list3[0]
Out[30]: 1

In [31]: list3[2]
Out[31]: [3, 4, 'hello']

In \[33]: list3[2\][2]
Out[33]: 'hello'

In \[34]: list3[2\][2] = 'goodbye'
In \[35]: list3[2\][2]
Out[35]: 'goodbye'
```

Sort the list below:

```py
In [36]: list4 = [5, 3, 4, 6, 1]
In [38]: list4.sort()
In [39]: list4
Out[39]: [1, 3, 4, 5, 6]
```

### Dictionaries

Using keys and indexing, grab the 'hello' from the following dictionaries:

```py
# Grab 'hello'
In [42]: d = {'simple_key':'hello'}
In [43]: d['simple_key']
Out[43]: 'hello'

# Getting a little tricker
# Grab hello
In [52]: d = {'k1':[{'nest_key':['this is deep',['hello']]}]}

In [53]: d['k1']
Out[53]: [{'nest_key': ['this is deep', ['hello']]}]

In \[54]: d['k1'\][0]
Out[54]: {'nest_key': ['this is deep', ['hello']]}

In \[55]: d['k1'\][0]['nest_key']
Out[55]: ['this is deep', ['hello']]

In \[56]: d['k1'\][0]\['nest_key'\][0]
Out[56]: 'this is deep'

In \[57]: d['k1'\][0]\['nest_key'\][1]
Out[57]: ['hello']

# This will be hard and annoying!
In [58]: d = {'k1':[1,2,{'k2':['this is tricky',{'tough':[1,2,['hello']]}]}]}

In [59]: d['k1']
Out[59]: [1, 2, {'k2': ['this is tricky', {'tough': [1, 2, ['hello']]}]}]

In \[60]: d['k1'\][2]
Out[60]: {'k2': ['this is tricky', {'tough': [1, 2, ['hello']]}]}

In \[61]: d['k1'\][2]['k2']
Out[61]: ['this is tricky', {'tough': [1, 2, ['hello']]}]

In \[62]: d['k1'\][2]\['k2'\][1]
Out[62]: {'tough': [1, 2, ['hello']]}

In \[63]: d['k1'\][2]\['k2'\][1]['tough']
Out[63]: [1, 2, ['hello']]

In \[64]: d['k1'\][2]\['k2'\][1]\['tough'\][2]
Out[64]: ['hello']
```

#### Can you sort a dictionary? Why or why not?

No, because dictionaries are not ordered.

### Tuples

#### What is the major difference between tuples and lists?

Tuples are immutable.

#### How do you create a tuple?

Just like lists, but with ( ) instead of [ ]

### Sets

#### What is unique about a set?

Replicate values are ignored
Use a set to find the unique values of the list below:

```py
In [67]: list5 = [1,2,2,33,4,4,11,22,3,3,2]

In [68]: set(list5)
Out[68]: {1, 2, 3, 4, 11, 22, 33}
```

### Booleans

For the following quiz questions, we will get a preview of comparison operators. In the table below, a=3 and b=4.

| Operator | Description | Example |
| -------- | ----------------------------------------------------------------------------------------------------------------- | -------------------- |
| ==       | If the values of two operands are equal, then the condition becomes true.                                         | (a == b) is not true |
| !=       | If values of two operands are not equal, then condition becomes true.                                             | (a != b) is true     |
| >        | If the value of left operand is greater than the value of right operand, then condition becomes true.             | (a > b) is not true  |
| <        | If the value of left operand is less than the value of right operand, then condition becomes true.                | (a < b) is true      |
| >=       | If the value of left operand is greater than or equal to the value of right operand, then condition becomes true. | (a >= b) is not true |
| <=       | If the value of left operand is less than or equal to the value of right operand, then condition becomes true.    | (a <= b) is true     |

What will be the resulting Boolean of the following pieces of code (answer first then check by typing it in!)

Answer before running cell

```py
In [1]: 2 > 3
Out [1]:False
In [2]: 3 <= 2
Out [2]: False
In [3]: 3 == 2.0
Out [3]: False
In [4]: 3.0 == 3
Out [4]: True
In [5]: 4**0.5 != 2
Out [5]: False
```

Final Question: What is the boolean output of the cell below?

```py
# two nested lists
In [1]: l_one = [1,2,[3,4]]
In [2]: l_two = [1,2,{'k1':4}]


# True or False?
In \[3]: l_one[2\][0] >= l_two\[2\]['k1']
Out [3]: False
```

---
