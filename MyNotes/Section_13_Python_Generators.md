# Section 13: Python Generators

## Lesson 83: Generators with Python

Generators are functions that can send back a value and later return and pick up where they left off.
The main difference is the use of the `yield` statement
The main advantage is that instead of having to compute an entire series of values up front, the generator computes one value and waits until the next value is called for.
This is what the `range` function does

```py
In [1]: def create_cubes(n):
  ...:   result = []
  ...:   for x in range(n):
  ...:     result.append(x**3)
  ...:   return result
  ...:
  ...:

In [2]: create_cubes(10)
Out[2]: [0, 1, 8, 27, 64, 125, 216, 343, 512, 729]

In [3]: for x in create_cubes(10):
  ...:   print(x)
0
1
8
27
64
125
216
343
512
729

In [4]: def create_cubes(n):  # this is more efficient
  ...:   for x in range(n):  # instead of keeping all nums in memory,
  ...:     yield x**3        # only the most recent num is kept in memory
  ...:
  ...:
  ...: for x in create_cubes(10):
  ...:   print(x)
  ...:
  ...:
0
1
8
27
64
125
216
343
512
729

In [5]: create_cubes(10)  # calling it doesn't actually produce a result
Out[5]: <generator object create_cubes at 0x000001894111F408>

In [6]: def gen_fib(n):
  ...:
  ...:   a = 1
  ...:   b = 1
  ...:   for i in range(n):
  ...:     yield a
  ...:     a, b = b, a+b
  ...:
  ...:
  ...: for num in gen_fib(10):
  ...:   print(num)
  ...:
  ...:
1
1
2
3
5
8
13
21
34
55

In [7]: def gen_fib(n):  # this does the same thing, but far less efficiently
  ...:
  ...:   a = 1
  ...:   b = 1
  ...:   output = []
  ...:
  ...:   for i in range(n):
  ...:     output.append(a)
  ...:     a, b = b, a+b
  ...:   return output
  ...:
```

### next() and iter() built-in functions

```py
In [8]: def simple_gen():
  ...:   for x in range(3):
  ...:     yield x
  ...:
  ...:
  ...: for num in simple_gen():
  ...:   print(num)
  ...:
  ...:
0
1
2

In [9]: g = simple_gen()

In [10]: g
Out[10]: <generator object simple_gen at 0x000001894111F570>

In [11]: print(next(g))  # only calls first result
0

In [12]: print(next(g))  # then next....
1

In [13]: print(next(g))
2

In [14]: print(next(g))
Traceback (most recent call last):

File "<ipython-input-14-1dfb29d6357e>", line 1, in <module>
print(next(g))

StopIteration

# error because we reached the end
# for loops automatically catch this error

In [17]: s = 'hello'

In [18]: for letter in s:
  ...:   print(letter)
h
e
l
l
o

In [19]: next(s)
Traceback (most recent call last):

File "<ipython-input-19-61c30b5fe1d5>", line 1, in <module>
next(s)

TypeError: 'str' object is not an iterator

In [20]: s_iter = iter(s)

In [21]: next(s_iter)
Out[21]: 'h'

In [22]: next(s_iter)
Out[22]: 'e'

In [23]: next(s_iter)
Out[23]: 'l'
```

---

## Lesson 84-85: Generators Homework

### Problem 1

Create a generator that generates the squares of numbers up to some number N.

```py
def gensquares(N):
  for y in range(0,N):
    yield y**2

for x in gensquares(10):
  print(x)

0
1
4
9
16
25
36
49
64
81
```

### Problem 2

Create a generator that yields "n" random numbers between a low and high number (that are inputs).
Note: Use the random library.

```py
def rand_num(low, high, n):
  import random
  for i in range(n):
    yield random.randint(low, high)


for num in rand_num(1, 10, 12):
  print(num)


9
4
5
3
6
7
8
9
6
6
2
3
```

### Problem 3

Use the iter() function to convert the string below into an iterator:

```py
s = 'hello'
s_iter = iter(s)
next(s_iter)
Out[7]: 'h'

next(s_iter)
Out[8]: 'e'

next(s_iter)
Out[9]: 'l'
```

### Problem 4

Explain a use case for a generator using a yield statement where you would not want to use a normal function with a return statement.

#### Answer:

When you don’t want all numbers to be calculated right away to save memory.

### Extra Credit!

Can you explain what *gencomp* is in the code below? (Note: We never covered this in lecture! You will have to do some Googling/Stack Overflowing!)

```py
my_list = [1,2,3,4,5]

gencomp = (item for item in my_list if item > 3)

for item in gencomp:
  print(item)
4
5
```

Seems straightforward. It checks to see if each item is >3 before adding it to list.

---
