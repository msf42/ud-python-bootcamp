# Section 10: Errors and Exceptions Handling

## 71 - Errors and Exceptions Handling

Any type of error in your code will cause the entire script to stop. **Error Handling** can be used to let the script continue with other code, even if there is an error

### Three keywords:

- **try** - this block of code to be attempted (may lead to error)
- **except** - block of code will execute in case there is an error in **try** block
- **finally** - a final block of code to be executed, regardless of error

```py
In [5]: def add(n1, n2):
  ...:     print(n1+n2)
  ...:

In [6]: add(10, 20)
30

In [7]: number1 = 10

In [8]: number2 = input("Please provide a number: ")

Please provide a number: 20        # recall this will be a string by default

In [9]: add(number1, number2)
Traceback (most recent call last):

  File "<ipython-input-9-ad6929989779>", line 1, in <module>
  add(number1, number2)

  File "<ipython-input-5-2f28b0828f25>", line 2, in add
  print(n1+n2)

TypeError: unsupported operand type(s) for +: 'int' and 'str'     # <<==

In [12]: try:
  ...:     # Want to try this code
  ...:     # May have an error
  ...:     result = 10 + 10
  ...: except:
  ...:     print("Hey it looks like you are not adding correctly!")
  ...:

In [13]: result        # works fine
Out[13]: 20

In [14]: try:
  ...:     # Want to try this code
  ...:     # May have an error
  ...:     result = 10 + '10'
  ...: except:
  ...:     print("Hey it looks like you are not adding correctly!")
  ...:
Hey it looks like you are not adding correctly!        # tried int + str

In [15]: try:
  ...:     # Want to try this code
  ...:     # May have an error
  ...:     result = 10 + '10'
  ...: except:
  ...:     print("Hey it looks like you are not adding correctly!")
  ...: else:
  ...:     print("Add went well!")
  ...:     print(result)
  ...:
Hey it looks like you are not adding correctly!        # same result

In [16]: try:
  ...:     # Want to try this code
  ...:     # May have an error
  ...:     result = 10 + 10
  ...: except:
  ...:     print("Hey it looks like you are not adding correctly!")
  ...: else:
  ...:     print("Add went well!")
  ...:     print(result)
  ...:
Add went well!
20
```

#### Another example, using `finally`

```py
In [17]: try:
  ...:     f = open('testfile', 'w')
  ...:     f.write("Write a test line")
  ...: except TypeError:
  ...:     print("There was a type error!")
  ...: except OSError:
  ...:     print("Hey!You have an OS Error!")
  ...: finally:
  ...:     print("I always run")
  ...:
I always run

In [18]: try:
  ...:     f = open('testfile', 'r')
  ...:     f.write("Write a test line")
  ...: except TypeError:
  ...:     print("There was a type error!")
  ...: except OSError:
  ...:     print("Hey!You have an OS Error!")
  ...: finally:
  ...:     print("I always run")
  ...:
Hey!You have an OS Error!
I always run

In [19]: try:
  ...:     f = open('testfile', 'r')
  ...:     f.write("Write a test line")
  ...: except TypeError:
  ...:     print("There was a type error!")
  ...: except:
  ...:     print("All other errors!")
  ...: finally:
  ...:     print("I always run")
  ...:
All other errors!
I always run
```

#### When you need a specific type of input from the user:

```py
In [20]: def ask_for_int():
  ...:     try:
  ...:         result = int(input("Please provide a number: "))
  ...:     except:
  ...:         print("Whoops! That is not a number")
  ...:     finally:
  ...:         print("End of try/except/finally")
  ...:

In [21]: ask_for_int()

Please provide a number: 20
End of try/except/finally

In [22]: ask_for_int()

Please provide a number: word
Whoops! That is not a number
End of try/except/finally

In [23]: def ask_for_int():
  ...:     while True:
  ...:         try:
  ...:             result = int(input("Please provide a number: "))
  ...:         except:
  ...:             print("Whoops! That is not a number")
  ...:             continue
  ...:         else:
  ...:             print("yes thank you")
  ...:             break
  ...:         finally:
  ...:             print("End of try/except/finally")
  ...:             print("I will always run at the end.")
  ...:

In [24]: ask_for_int()

Please provide a number: 20
yes thank you
End of try/except/finally
I will always run at the end.

In [25]: ask_for_int()

Please provide a number: easdf
Whoops! That is not a number
End of try/except/finally
I will always run at the end.

Please provide a number: sdfasdf
Whoops! That is not a number
End of try/except/finally
I will always run at the end.

Please provide a number: 333
yes thank you
End of try/except/finally
I will always run at the end.
----------

## 72/73 - Errors and Exceptions Homework

### Problem 1

Handle the exception thrown by the code below by using try and except blocks.

```py
In [1]:
for i in ['a','b','c']:
  print(i**2)
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-1-c35f41ad7311> in <module>()
    1 for i in ['a','b','c']:
----> 2     print(i**2)

TypeError: unsupported operand type(s) for ** or pow(): 'str' and 'int'
```

#### Answer

```py
In [1]: try:
  ...:     for i in ['a','b','c']:
  ...:         print(i**2)
  ...: except TypeError:
  ...:     print("Only integers are acceptable!")
  ...:
  ...:
Only integers are acceptable!
```

### Problem 2

Handle the exception thrown by the code below by using `try` and `except` blocks. Then use a `finally` block to print 'All Done.'

```py
x = 5
y = 0

z = x/y
---------------------------------------------------------------------------
ZeroDivisionError                         Traceback (most recent call last)
<ipython-input-2-6f985c4c80dd> in <module>()
    2 y = 0
    3
----> 4 z = x/y

ZeroDivisionError: division by zero
```

#### Answer:

```py
In [2]: x = 5
  ...: y = 0
  ...: try:
  ...:     z = x/y
  ...: except:
  ...:     if y == 0:
  ...:         print("y can NOT equal zero!")
  ...: finally:
  ...:     print("all done!")
  ...:
  ...:
y can NOT equal zero!
all done!
```

### Problem 3

Write a function that asks for an integer and prints the square of it. Use a `while` loop with a `try`, `except`, `else` block to account for incorrect inputs.

```py
def ask():
  pass

In [4]:

ask()

Input an integer: null
An error occurred! Please try again!
Input an integer: 2
Thank you, your number squared is:  4
```

#### Answer

```py
In [8]: def ask():
  ...:     while True:
  ...:         try:
  ...:             x = int(input("Enter a number:"))
  ...:         except:
  ...:             print("Please try again \n")
  ...:             continue
  ...:         else:
  ...:             break
  ...:     print(x**2)
  ...:
  ...:

In [9]: ask()

Enter a number:x
Please try again

Enter a number:sad
Please try again

Enter a number:3
9
```

----------

## 74 - Pylint Overview

**Pylint** and **Unitest**
**Pylint** is a library that looks at your code and reports back possible issues.
**Styling** - Python follows a set of convention rules known as **PEP8**

### First Try

#### /simple1.py

```py
a = 1
b = 2
print(a)
print(B)
```

#### Result of pylint analysis

```py
  msf42> pylint3 simple1.py
No config file found, using default configuration
************* Module simple1
C:  1, 0: Missing module docstring (missing-docstring)
C:  1, 0: Constant name "a" doesn't conform to UPPER_CASE naming style (invalid-name)
C:  2, 0: Constant name "b" doesn't conform to UPPER_CASE naming style (invalid-name)
E:  4, 6: Undefined variable 'B' (undefined-variable)

---------------------------------------------------------------------
Your code has been rated at -10.00/10 (previous run: -2.50/10, -7.50)
```

### After Corrections

#### /simple1.py

```py
'''

A very simple script

'''

def myfunc():
  '''
  Docstring
  '''
  first = 1
  second = 2
  print(first)
  print(second)

myfunc()

#### Result of new pylint analysis

  msf42 > pylint3 simple1.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
```

----------

## 75 - Running Tests with the Unittest Library

### Unitest

is a built-in library that will allow you to test your own programs and ensure you are getting desired results

#### /cap.py

```py
def cap_text(text):
  '''
  input a string
  output the capitalized string
  '''
  return text.capitalize()
```

#### /test_cap.py

```py
import unittest  # first import unittest
import cap       # then import file you are testing

class TestCap(unittest.TestCase):  # inherit from unittest.TestCase
  # write a series of methods to test your file

  def test_one_word(self):
    text = 'python'
    result = cap.cap_text(text)  # text fed into cap.py
    self.assertEqual(result, 'Python')  # results should be...

  def test_multiple_words(self):
    text = 'monty python'
    result = cap.cap_text(text)
    self.assertEqual(result, 'Monty Python')

if __name__=='__main__':
  unittest.main()
```

#### Results

```py
msf42 > python3.6 test_cap.py
F.
======================================================================
FAIL: test_multiple_words (__main__.TestCap)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "test_cap.py", line 14, in test_multiple_words
  self.assertEqual(result, 'Monty Python')
AssertionError: 'Monty python' != 'Monty Python'
- Monty python
?       ^
+ Monty Python
?       ^

----------------------------------------------------------------------
Ran 2 tests in 0.001s

FAILED (failures=1)
```

#### New /cap.py

```py
def cap_text(text):
  '''
  input a string
  output the capitalized string
  '''
  return text.title()  # changed capitalize to title
```

#### Results

```py
msf42 > python3.6 test_cap.py
..
----------------------------------------------------------------------
Ran 2 tests in 0.000s
```

----------
