# Section 09: Modules and Packages

## 68 - Pip Install and PyPi

When Python or Anaconda are installed, **pip** is automatically installed. It can be used to install packages through the command line, directly from the PyPi repositories.
Often helpful to google "Python package for x" . This usually brings up the PyPi page, with package information.
Installation is typically

```bash
pip install packagename
```

---

## 69 - Modules and Packages

- **Modules** are just .py scripts that you call in another .py script
- **Packages** are just collections of modules
- **Folder:** MyMainPackage
- **Subfolder**: SubPackage
  - Both must include file called __init__.py

### First Lesson

#### /mymodule.py

```py
def my_func():
  print("Hey I am in my module.py")
```

#### /myprogram.py

```py
from mymodule import my_func
my_func()
```

#### Running in the command line

```py
steve@steve-lubu:~/pCloudDrive/_ACTIVE/Tech/Python_Bootcamp/09-69_Modules_and_Packages$ python myprogram.py
Hey I am in my module.py
```

### Second Lesson

To let Python know that a directory is a *package*, we need to create a file called `__init__.py`

#### /some_main_script.py

```py
def report_main():
  print("Hey I am in some_main_script in main package")
```

#### /my_subscript.py

```py
def sub_report():
  print("Hey I am a function inside my subscript")
```

#### /myprogram.py

```py
from MyMainPackage import some_main_script
from MyMainPackage.SubPackage import mysubscript

some_main_script.report_main() # executes report_main from some_main_script

mysubscript.sub_report()        # executes sub_report from my_subscript
```

#### Running in the command line

```py
9_Modules_and_Packages$ python myprogram.py
Hey I am in some_main_script in main package
Hey I am a function inside my subscript
```

---

## 70 - `__name__` and `__main__`

The following code is often found in Python:

```py
if __name__=="__main__":
```

Sometimes when you import from a module, you need to know if a modules function is being used as an import, or if you are using the original `.py` file of that module.

In Python, when you call a script at the command line, *everything* at indentation level 0 gets run (so this means everything). There is no "main" function that gets run instead, as in other languages.

Python has a built-in variable, `__name__`

When you run a script directly, as in `python3.7 one.py`, Python automatically assigns `__main__` to `__name__`, as in `__name__ = "__main__"` (note the single equals sign; this is a variable assignment)

This allows us to test whether the script is being called *directly* by adding the `if` statement:

```py
  if __name__=="__main__"
```

### Command Line Results:

```py
steve@steve-xubu:$ python3.7 one.py
TOP LEVEL IN ONE.PY
ONE.PY is being run directly!

steve@steve-xubu:$ python3.7 two.py
TOP LEVEL IN ONE.PY
ONE.PY has been imported!
TOP LEVEL IN TWO.PY
FUNC() IN ONE.PY
TWO.PY is being run directly!
```

### The reasoning behind this is code organization:

```py
#steves_script.py

def func1():
  pass

def func2():
  pass

def func3():
  pass

if __name__ == '__main__':
  func2()
  func3()
  func1()
```

---
