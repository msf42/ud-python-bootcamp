# Section 16: Advanced Python Objects and Data Structures

## 97: Advanced Numbers

### Hexadecimal

```py
hex(4200)
Out[54]: '0x1068'
```

### Binary

```py
bin(42)
Out[55]: '0b101010'
```

### Exponentials

```py
pow(2, 10)  # can add a third argument, to be treated as mod - (x ** y) % z
Out[56]: 1024
```

### Absolute Value

```py
abs(-3)
Out[57]: 3
```

### Round

```py
round(395, -2)
Out[58]: 400

round(3.14159, 2)
Out[59]: 3.14
```

---

## 98: Advanced Strings

```py
s = 'hello world'
```

### Changing Case

```py
s.capitalize()
Out[2]: 'Hello world'

s.lower()
Out[3]: 'hello world'

s.upper()
Out[4]: 'HELLO WORLD'
```

### Location and Counting

```py
s.count('o')
Out[6]: 2

s.count('l')
Out[7]: 3

s.find('o')
Out[8]: 4
```

### Formatting

```py
s.center(20, 'z')
Out[9]: 'zzzzhello worldzzzzz'

'hello\thi'.expandtabs()
Out[10]: 'hello   hi'
```

### is Check Methods

```py
s
Out[15]: 'hello world'

s.isalnum()
Out[16]: False

s.isalpha()
Out[17]: False

s.islower()
Out[18]: True

s.isspace()
Out[19]: False

s.istitle()
Out[20]: False

s.isupper()
Out[21]: False

s.endswith('o')
Out[22]: False
```

### Built-In Regular Expressions

```py
s.split('e')
Out[23]: ['h', 'llo world']

s.partition('l')
Out[24]: ('he', 'l', 'lo world')
```

---

## 99: Advanced Sets

```py
s = set()

**add**

s.add(1)

s.add(2)

s
Out[4]: {1, 2}
```

### clear

```py
s.clear()

s
Out[6]: set()
```

### copy

```py
s = {1, 2, 3}

sc = s.copy()

sc
Out[9]: {1, 2, 3}

s
Out[10]: {1, 2, 3}

s.add(4)

s
Out[12]: {1, 2, 3, 4}

sc
Out[13]: {1, 2, 3}
```

### difference

```py
s.difference(sc)
Out[14]: {4}
```

### difference_update

```py
s1 = {1, 2, 3}

s2 = {1, 4, 5}

s1.difference_update(s2)
# returns set1 after removing elements found in set2
s1
Out[18]: {2, 3}
```

### discard

```py
s
Out[19]: {1, 2, 3, 4}

s.discard(2)

s
Out[21]: {1, 3, 4}
```

### intersection and intersection_update

```py
s1 = {1, 2, 3}

s2 = {1, 2, 4}

s1.intersection(s2)
Out[24]: {1, 2}

s1
Out[26]: {1, 2, 3}

s1.intersection_update(s2)

s1
Out[28]: {1, 2}
```

### isdisjoint

```py
s1 = {1,2}
s2 = {1,2,4}
s3 = {5}

s1.isdisjoint(s2)  # returns True if the 2 sets have a null intersection
Out[30]: False

s1.isdisjoint(s3)
Out[31]: True
```

### issubset

```py
s1.issubset(s2)
Out[35]: True

s2.issubset(s1)
Out[36]: False
```

### issuperset

```py
s1.issuperset(s2)
Out[33]: False

s2.issuperset(s1)
Out[34]: True
```

### symmetric_difference and symmetric_update

```py
s1
Out[37]: {1, 2}

s2
Out[38]: {1, 2, 4}

s1.symmetric_difference(s2)
Out[39]: {4}
```

### union

```py
s1.union(s2)
Out[40]: {1, 2, 4}
```

### update

```py
s1.update(s2)

s1
Out[42]: {1, 2, 4}
```

---

## 100: Advanced Dictionaries

### Dictionary Comprehension

A quick way to create a dictionary

```py
{x:x**2 for x in range(10)}

Out[1]: {0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81}

# using zip - zips a, b... into range()

In [11]: {k:v**2 for k,v in zip(['a','b'], range(2))}
Out[11]: {'a': 0, 'b': 1}
```

### Iteration over keys, values, and items

```py
In [2]: d = {'k1':1, 'k2':2}

In [3]: for k in d.keys():
  ...:     print(k)
  ...:
k1
k2

In [4]: for v in d.values():
  ...:     print(v)
  ...:
1
2

In [5]: for item in d.items():
  ...:     print(item)
  ...:
('k1', 1)
('k2', 2)
```

### Viewing keys, values, and items

```py
In [6]: key_view = d.keys()

In [7]: key_view
Out[7]: dict_keys(['k1', 'k2'])

In [8]: d['k3'] = 3

In [9]: d
Out[9]: {'k1': 1, 'k2': 2, 'k3': 3}

In [10]: key_view
Out[10]: dict_keys(['k1', 'k2', 'k3'])
```

---

## 101: Advanced Lists

### append, count

```py
In [13]: list1 = [1, 2, 3]

In [14]: list1.append(4)

In [15]: list1.count(10)
Out[15]: 0

In [16]: list1.count(2)
Out[16]: 1
```

### extend, index, insert

```py
In [17]: x = [1, 2, 3]

In [18]: x.append([4,5])

In [19]: print(x)
[1, 2, 3, [4, 5]]

In [20]: x = [1, 2, 3]

In [21]: x.extend([4,5])

In [22]: x
Out[22]: [1, 2, 3, 4, 5]

In [23]: list1.index(2)
Out[23]: 1

In [24]: list1.index(12)

---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-24-56b94ada72bf> in <module>
----> 1 list1.index(12)

ValueError: 12 is not in list

In [25]: list1
Out[25]: [1, 2, 3, 4]

In [26]: list1.insert(2, 'inserted')

In [27]: list1
Out[27]: [1, 2, 'inserted', 3, 4]
```

#### pop, remove, reverse, sort

```py
In [27]: list1
Out[27]: [1, 2, 'inserted', 3, 4]

In [28]: ele = list1.pop(1)

In [29]: list1
Out[29]: [1, 'inserted', 3, 4]

In [30]: ele
Out[30]: 2

In [31]: list1.remove('inserted')

In [32]: list1
Out[32]: [1, 3, 4]

In [33]: list2 = [1, 2, 3, 4, 5]

In [34]: list2.remove(3)

In [35]: list2
Out[35]: [1, 2, 4, 5]

In [54]: l = [1, 2, 3, 4, 3]

In [55]: l.remove(3)             # only removes first occurrence

In [56]: l
Out[56]: [1, 2, 4, 3]


In [36]: list2.reverse()

In [37]: list2
Out[37]: [5, 4, 2, 1]

In [38]: list2.sort()

In [39]: list2
Out[39]: [1, 2, 4, 5]

In [40]: list2.sort(reverse=True)

In [41]: list2
Out[41]: [5, 4, 2, 1]
```

### Be careful with assignment!

```py
In [42]: x = 'hello world'

In [43]: y = x.upper()

In [44]: print(y)

HELLO WORLD

In [45]: x = [1,2,3]

In [46]: y = x.append(4)

In [47]: print(y)
None

In [48]: # append works in place only

In [49]: x = [1,2,3]

In [50]: y = x.copy()

In [51]: y.append(4)

In [52]: print(y)
[1, 2, 3, 4]

In [53]: print(x)
[1, 2, 3]
```

---

## 102-3: Advanced Python Objects Assessment Test

### Advanced Numbers

#### Problem 1: Convert 1024 to binary and hexadecimal representation

```py
In [1]: hex(1024)
Out[1]: '0x400'

In [2]: bin(1024)
Out[2]: '0b10000000000'
```

#### Problem 2: Round 5.23222 to two decimal places

```py
In [3]: round(5.2322, 2)
Out[3]: 5.23
```

### Advanced Strings

#### Problem 3: Check if every letter in the string s is lower case

```py
In [7]: s = 'hello how are you Mary, are you feeling okay?'

In [8]: s == s.lower()   # or s.islower()
Out[8]: False
```

#### Problem 4: How many times does the letter 'w' show up in the string below?

```py
In [9]: s = 'twywywtwywbwhsjhwuwshshwuwwwjdjdid'

In [10]: s.count('w')
Out[10]: 12
```

### Advanced Sets

#### Problem 5: Find the elements in set1 that are not in set2:

```py
In [11]: set1 = {2,3,1,5,6,8}
  ...: set2 = {3,1,7,5,6,8}

In [12]: set1.difference(set2)
Out[12]: {2}
```

#### Problem 6: Find all elements that are in either set:

```py
In [14]: set1.union(set2)
Out[14]: {1, 2, 3, 5, 6, 7, 8}
```

### Advanced Dictionaries

#### Problem 7: Create this dictionary: {0: 0, 1: 1, 2: 8, 3: 27, 4: 64} using a dictionary comprehension.

```py
In [16]: {x:x**3 for x in range(5)}
Out[16]: {0: 0, 1: 1, 2: 8, 3: 27, 4: 64}
```

### Advanced Lists

#### Problem 8: Reverse the list below:

```py
In [ ]:

In [17]: list1 = [1,2,3,4]

In [18]: list1.reverse()

In [19]: list1
Out[19]: [4, 3, 2, 1]
```

#### Problem 9: Sort the list below:

```py
In [22]: list2 = [3,4,2,5,1]

In [23]: list2.sort()

In [24]: list2
Out[24]: [1, 2, 3, 4, 5]
```

---
