# Section 12: Python Decorators

## Lesson 81: Decorators with Python Overview

Decorators allow you to ‘decorate’ a function.
Suppose you create a function, then want to add more content to it. You now have two options:

1. You can add new functionality to your old code
2. You can create a new function that contains the old code, then add new code to it.

But what if you wanted to delete that new functionality? You would then need to delete the extra code, or keep a copy of the old code.

You may want to keep both the old function and the new function.

This is where decorators come in. They add that extra functionality to an already existing function. They use the `@` function and are placed at the top of the operator.

The idea is pretty abstract, so this lesson goes through the whole process step by step.

Here is the format:

```py
@some_decorator
def simple_func():
    # do some stuff
    # return something
```

Reminder: functions are objects that can be passed into other objects

```py
In [1]: def func():  # a simple function
    ...:   return 1
    ...:
    ...:

In [2]: func()
Out[2]: 1

In [3]: func
Out[3]: <function __main__.func()>

In [4]: def hello():
    ...:   return "Hello!"
    ...:
    ...:

In [5]: hello()
Out[5]: 'Hello!'

In [6]: hello
Out[6]: <function __main__.hello()>

In [7]: greet = hello

In [8]: greet()
Out[8]: 'Hello!'

In [9]: del hello  # deleting hello

In [10]: greet()  # greet still exists
Out[10]: 'Hello!'

In [11]: hello()  # but hello does not
Traceback (most recent call last):

File "<ipython-input-11-a75d7781aaeb>", line 1, in <module>
hello()

NameError: name 'hello' is not defined


In [12]: def hello(name='Jose'):
    ...:   print('The hello() function has been executed')
    ...:

In [13]: hello()
The hello() function has been executed


In [15]: def hello(name='Jose'):
    ...:   print('The hello() function has been executed')
    ...:
    ...:   def greet():
    ...:     return '\t This is the greet function inside hello'
    ...:

In [16]: hello()
The hello() function has been executed

In [17]: def hello(name='Jose'):
    ...:   print('The hello() function has been executed')
    ...:
    ...:   def greet():
    ...:     return '\t This is the greet function inside hello'
    ...:
    ...:   print(greet())
    ...:

In [18]: hello()
The hello() function has been executed
    This is the greet function inside hello

In [19]: def hello(name='Jose'):
    ...:   print('The hello() function has been executed')
    ...:
    ...:   def welcome():
    ...:     return '\t This is welcome inside hello'
    ...:
    ...:   def greet():
    ...:     return '\t This is the greet function inside hello'
    ...:
    ...:   print(greet())
    ...:   print(welcome())
    ...:

In [20]: hello()
The hello() function has been executed
    This is the greet function inside hello
    This is welcome inside hello

In [21]: def hello(name='Jose'):
    ...:   print('The hello() function has been executed')
    ...:
    ...:   def welcome():
    ...:     return '\t This is welcome inside hello'
    ...:
    ...:   def greet():
    ...:     return '\t This is the greet function inside hello'
    ...:
    ...:   print(greet())
    ...:   print(welcome())
    ...:   print('This is the end of the hello function')
    ...:

In [22]: hello()
The hello() function has been executed
    This is the greet function inside hello
    This is welcome inside hello
This is the end of the hello function

In [24]: welcome()
Traceback (most recent call last):

File "<ipython-input-24-a401d7101853>", line 1, in <module>
welcome()

NameError: name 'welcome' is not defined  # welcome only exists inside hello

In [27]: def hello(name='Jose'):
    ...:   print('The hello() function has been executed')
    ...:
    ...:   def welcome():
    ...:     return '\t This is welcome inside hello'
    ...:
    ...:   def greet():
    ...:     return '\t This is the greet function inside hello'
    ...:
    ...:   print("I am going to return a function!!")
    ...:
    ...:   if name =='Jose':
    ...:     return greet
    ...:   else:
    ...:     return welcome
    ...:

In [29]: hello()
The hello() function has been executed
I am going to return a function!!
Out[29]: <function __main__.hello.<locals>.greet()>

In [30]: my_new_func = hello('Jose')
The hello() function has been executed
I am going to return a function!!

In [31]: my_new_func
Out[31]: <function __main__.hello.<locals>.greet()>

In [32]: my_new_func()
Out[32]: '\t This is the greet function inside hello'

In [33]: def cool():
    ...:
    ...:   def supercool():
    ...:     return 'I am very cool'
    ...:   return supercool
    ...:

In [39]: some_func = cool()

In [40]: some_func
Out[40]: <function __main__.cool.<locals>.supercool()>

In [41]: some_func()

Out[41]: 'I am very cool'

In [42]: def hello():
    ...: return 'Hi Jose!'
    ...:
    ...:

In [43]: def other(some_def_func):
    ...:   print('Other code runs here')
    ...: print(some_def_func())
    ...:

In [44]: other(hello) # just passing in raw function, not executing
Other code runs here
Hi Jose!

In [45]: def new_decorator(original_func):
    ...:
    ...:   def wrap_func():
    ...:
    ...:     print('some extra code')
    ...:
    ...:   original_func()
    ...:
    ...:     print('Some extra code, after origninal function')
    ...:
    ...:   return wrap_func
    ...:
    ...:

In [46]: def func_needs_dec():
    ...: print("I want to be decorated")
    ...:

In [47]: func_needs_dec()
I want to be decorated

In [51]: def new_decorator(original_func):
    ...:
    ...:   def wrap_func():
    ...:
    ...:     print('some extra code')
    ...:
    ...:   original_func()
    ...:
    ...:   print('Some extra code, after origninal function')
    ...:
    ...:     return wrap_func
    ...:
    ...:

In [52]: decorated_func = new_decorator(func_needs_dec)

In [53]: decorated_func()
some extra code
I want to be decorated
Some extra code, after origninal function

    # decorated_func = new_decorator(func_needs_dec)
    # is the same as

In [57]: @new_decorator
    ...: def func_needs_dec():
    ...:   print("I want to be decorated")
    ...:

In [60]: func_needs_dec()
some extra code
I want to be decorated
Some extra code, after origninal function
```

---

## Lesson 82: Decorators Homework

Decorators won’t be used until much later, as they are pretty advanced.

### Some resources:

- [flask](http://flask.pocoo.org/)
- [decorators](http://flask.pocoo.org/docs/0.12/patterns/viewdecorators/)
- [django](https://www.djangoproject.com/)

#### Also for some additional info:

A framework is a type of software library that provides generic functionality which can be extended by the programmer to build applications. Flask and Django are good examples of frameworks intended for web development.

A framework is distinguished from a simple library or API. An API is a piece of software that a developer can use in his or her application. A framework is more encompassing: your entire application is structured around the framework (i.e. it provides the framework around which you build your software).

---
