# Section 04: Python Comparison Operators

## 31 - Comparison Operators in Python

### Very straightforward comparisons

```py
Out[1]: True

In [2]: 2 == 1
Out[2]: False

In [3]: 'hello' == 'bye'
Out[3]: False

In [4]: 'Bye' == 'bye'  ## case counts
Out[4]: False

In [5]: '2' == 2  ## type counts
Out[5]: False

In [6]: 2.0 == 2  #floats/integers don't matter
Out[6]: True

In [7]: 3 != 3
Out[7]: False

In [8]: 4 != 5
Out[8]: True

In [9]: 2 > 1
Out[9]: True

In [10]: 1 > 2
Out[10]: False

In [11]: 1 < 2
Out[11]: True

In [12]: 2 >= 2
Out[12]: True

In [13]: 4 <= 1
Out[13]: False
```

---

## 32 - Chaining Comparison Operators in Python with Logical Operators

### Logical operators allow us to combine comparisons

and, or, not

```py
In [15]: 1 < 2 < 3
Out[15]: True

In [16]: 1 < 2 > 3
Out[16]: False

In [17]: 1 < 2 and 2 > 3
Out[17]: False

In [18]: 1 < 2 and 2 < 3
Out[18]: True

In [19]: ('h' == 'h') and (2 == 2) ## () are optional
Out[19]: True

In [20]: 2 > 3 or 1 == 1
Out[20]: True

In [21]: not(1 ==1)  ## again, () optional
Out[21]: False
```

---
