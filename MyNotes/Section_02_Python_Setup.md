# Section 02: Python Setup

---

## 05 - Command Line Basics

```bash
pwd        # prints working directory
ls         # lists files in directory
cd Desktop # changes directory to desktop
clear      # clears the screen
cd ..      # moves up in directory
```

---

## 06 - Python Overview and Installing Python

- Developed in 1991
- Can be learned quickly
- Uses white spaces
  - easy to read
  - uses less code
- Huge amount of additional open-source libraries available
- Course uses Anaconda

---

## 07 - Running Python Code

### Three Main Environments:

- Text editors
  - Some have add-ons and plug-ins that can be used
  - Atom and Sublime are most common
- Full IDEs
  - Development Environments designed specifically for Python
  - Larger programs than text editors
  - PyCharm and Spyder are most common
- Notebook Environments
  - Great for learning because input and output can be seen next to each other
  - They use blocks of code called “cells”
  - Special file formats that are not .py
  - Most popular is Jupyter Notebook

---

## 08 - Getting the Notebooks and Course Material

[Course Data Link](https://github.com/Pierian-Data/Complete-Python-3-Bootcamp)

---

## 09 - Git & Github

- Git is a version control system
- A series of snapshots of code, known as ‘commits’
- Git can be used without Github, hosted locally on Dropbox, etc

---
