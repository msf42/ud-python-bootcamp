# Section 07: Milestone 1

---

## 54 - 58 - Tic-Tac-Toe Game

### Step 1:

#### Write a function that can print out a board. Set up your board as a list, where each index 1-9 corresponds with a number on a number pad, so you get a 3 by 3 board representation.

```py
def display_board(board):
  print("| {} | {} | {} |\n| {} | {} | {} |\n| {} | {} | {} |".format(board[1], board[2], board[3], board[4], board[5], board[6], board[7], board[8], board[9]))
```

### Test Step 1

```py
test_board = ['#','X','O','X','O','X','O','X','O','X']

display_board(test_board)

In [1]: def display_board(board):
  ...:     print("| {} | {} | {} |\n| {} | {} | {} |\n| {} | {} | {} |".format(board[1], board[2], board[3], board[4], board[5], board[6], board[7], board[8], board[9]))
  ...:
  ...:
  ...:
  ...: test_board = ['#','X','O','X','O','X','O','X','O','X']
  ...: display_board(test_board)
| X | O | X |
| O | X | O |
| X | O | X |
```

### Step 2:

#### Write a function that can take in a player input and assign their marker as 'X' or 'O'. Think about using while loops to continually ask until you get a correct answer.

```py
def player_input():
  marker = ''

  while marker == '':
    marker = input('Player 1: Choose X or O? ')

  if marker == 'X':
    return ('X', 'O')
  else:
    return ('O', 'X')
player_input()
```

### Test Step 2

```py
In [51]: def player_input():
  ...:     marker = ''
  ...:
  ...:     while marker == '':
  ...:         marker = input('Player 1: Choose X or O? ')
  ...:
  ...:     if marker == 'X':
  ...:         return ('X', 'O')
  ...:     else:
  ...:         return ('O', 'X')
  ...:
  ...:
  ...: player_input()
Player 1: Choose X or O? X
Out[51]: ('X', 'O')

In [52]: player_input()

Player 1: Choose X or O? O
Out[52]: ('O', 'X')
```

### Step 3:

#### Write a function that takes in the board list object, a marker ('X' or 'O'), and a desired position (number 1-9) and assigns it to the board.

```py
def  place_marker(board, marker, position):
  board[position] = marker
```

### Test Step 3

```py
place_marker(test_board,'$',8)
display_board(test_board)

In [4]: def  place_marker(board, marker, position):
  ...:     board[position] = marker
  ...:
  ...:
  ...: place_marker(test_board,'$',8)
  ...: display_board(test_board)
| X | O | X |
| O | X | O |
| X | $ | X |
```

### Step 4:

#### Write a function that takes in a board and a mark (X or O) and then checks to see if that mark has won.

```py
def win_check(board, mark):
  if mark == board[1] == board[2] == board[3]:
    return True
  elif mark == board[4] == board[5] == board[6]:
    return True
  elif mark == board[7] == board[8] == board[9]:
    return True
  elif mark == board[1] == board[4] == board[7]:
    return True
  elif mark == board[2] == board[5] == board[8]:
    return True
  elif mark == board[3] == board[6] == board[9]:
    return True
  elif mark == board[1] == board[5] == board[9]:
    return True
  elif mark == board[3] == board[5] == board[7]:
    return True
  else:
    return False
```

### Test Step 4

```py
win_check(test_board,'X') # should return True

In [5]: def win_check(board, mark):
  ...:     if mark == board[1] == board[2] == board[3]:
  ...:         return True
  ...:     elif mark == board[4] == board[5] == board[6]:
  ...:         return True
  ...:     elif mark == board[7] == board[8] == board[9]:
  ...:         return True
  ...:     elif mark == board[1] == board[4] == board[7]:
  ...:         return True
  ...:     elif mark == board[2] == board[5] == board[8]:
  ...:         return True
  ...:     elif mark == board[3] == board[6] == board[9]:
  ...:         return True
  ...:     elif mark == board[1] == board[5] == board[9]:
  ...:         return True
  ...:     elif mark == board[3] == board[5] == board[7]:
  ...:         return True
  ...:     else:
  ...:         return False
  ...: # Test Step 4
  ...: win_check(test_board,'X') # should return True
Out[5]: True
```

### Step 5:

#### Write a function that uses the random module to randomly decide which player goes first. You may want to lookup random.randint() Return a string of which player went first.

```py
import random

def choose_first():
  choice = random.randint(1,2)
  if choice == 1:
    return "Player 1"
  else:
    return "Player 2"

choose_first()
```

### Test Step 5

```py
In [7]: import random
  ...:
  ...: def choose_first():
  ...:     choice = random.randint(1,2)
  ...:     if choice == 1:
  ...:         return "Player 1"
  ...:     else:
  ...:         return "Player 2"
  ...:
  ...:
  ...: choose_first()
Out[7]: 'Player 1'

In [8]: choose_first()
Out[8]: 'Player 1'

In [9]: choose_first()
Out[9]: 'Player 2'
```

### Step 6:

#### Write a function that returns a boolean indicating whether a space on the board is freely available.

```py
blank_board = ['#', '_', '_', '_', '_', '_', '_', '_', '_', '_',]
def space_check(board, position):
  return board[position] != '_'

space_check(blank_board, 3)
```

### Test Step 6

```py
In [15]: blank_board = ['#', '_', '_', '_', '_', '_', '_', '_', '_', '_',]
  ...: def space_check(board, position):
  ...:     return board[position] != '_'
  ...:
  ...:
  ...: space_check(blank_board, 3)
Out[15]: False

In [16]: blank_board = ['#', '_', '_', 'X', '_', '_', '_', '_', '_', '_',]
  ...: def space_check(board, position):
  ...:     return board[position] != '_'
  ...:
  ...:
  ...: space_check(blank_board, 3)
Out[16]: True
```

### Step 7:

#### Write a function that checks if the board is full and returns a boolean value. True if full, False otherwise.

```py
blank_board = ['#', '_', '_', '_', '_', '_', '_', '_', '_', '_']
test_board = ['#','X','O','X','O','X','O','X','O','X']
def  full_board_check(board):
  for i in board:
    if i == '_':
      return False
    else:
      continue
  return True

full_board_check(test_board)

full_board_check(blank_board)
```

### Test Step 7

```py
In [39]: blank_board = ['#', '_', '_', '_', '_', '_', '_', '_', '_', '_']
  ...: test_board = ['#','X','O','X','O','X','O','X','O','X']
  ...: def  full_board_check(board):
  ...:     for i in board:
  ...:         if i == '_':
  ...:             return False
  ...:         else:
  ...:             continue
  ...:     return True
  ...:
  ...:
  ...: full_board_check(test_board)
Out[39]: True

In [40]: full_board_check(blank_board)
Out[40]: False
```

### Step 8:

#### Write a function that asks for a player's next position (as a number 1-9) and then uses the function from step 6 to check if it's a free position. If it is, then return the position for later use.

```py
test_board2 = ['#','X','_','X','O','X','O','X','O','X']
def player_choice(board):
  position = int(input('Please enter a number: '))
  while space_check(board, position) == True:
    position = int(input('That position is taken. Please choose another: '))
  return position
```

### Test Step 8

```py
player_choice(test_board2)
  ...: test_board2 = ['#','X','_','X','O','X','O','X','O','X']
  ...: def player_choice(board):
  ...:     position = int(input('Please enter a number: '))
  ...:     while space_check(board, position) == True:
  ...:         position = int(input('That position is taken. Please choose another: '))
  ...:     return position
  ...:
  ...:
  ...: player_choice(test_board2)

Please enter a number: 5

That position is taken. Please choose another: 2
Out[45]: 2
```

### Step 9:

#### Write a function that asks the player if they want to play again and returns a boolean True if they do want to play again.

```py
def replay():
  answer = input('Would you like to play again? (Y/N) ')
  if answer == 'Y':
    return True
  elif answer == 'N':
    return False
  else:
    return input('Please answer with Y or N: ')
replay()
```

### Test Step 9

```py
In [49]: def replay():
  ...:     answer = input('Would you like to play again? (Y/N) ')
  ...:     if answer == 'Y':
  ...:         return True
  ...:     elif answer == 'N':
  ...:         return False
  ...:     else:
  ...:         return input('Please answer with Y or N: ')
  ...:
  ...:
  ...: replay()

Would you like to play again? (Y/N) Y
Out[49]: True

In [50]: replay()

Would you like to play again? (Y/N) N
Out[50]: False
```

### Step 10:

#### Here comes the hard part! Use while loops and the functions you've made to run the game!

```py
def player_input():
  marker = ''

  while marker == '':
    marker = input('Player 1: Choose X or O? ')

  if marker == 'X':
    return ('X', 'O')
  else:
    return ('O', 'X')

import random
def choose_first():
  choice = random.randint(1,2)
  if choice == 1:
    return "Player 1"
  else:
    return "Player 2"

def display_board(board):
  print("| {} | {} | {} |\n| {} | {} | {} |\n| {} | {} | {} |".format(board[1], board[2], board[3], board[4], board[5], board[6], board[7], board[8], board[9]))

def space_check(board, position):
  return board[position] != '_'

def player_choice(board):
  position = int(input(turn + ' Please enter a number: '))
  while space_check(board, position) == True:
    position = int(input('That position is taken. Please choose another: '))
  return position

def  place_marker(board, marker, position):
  board[position] = marker

def win_check(board, mark):
  if mark == board[1] == board[2] == board[3]:
    return True
  elif mark == board[4] == board[5] == board[6]:
    return True
  elif mark == board[7] == board[8] == board[9]:
    return True
  elif mark == board[1] == board[4] == board[7]:
    return True
  elif mark == board[2] == board[5] == board[8]:
    return True
  elif mark == board[3] == board[6] == board[9]:
    return True
  elif mark == board[1] == board[5] == board[9]:
    return True
  elif mark == board[3] == board[5] == board[7]:
    return True
  else:
    return False

def  full_board_check(board):
  for i in board:
    if i == '_':
      return False
    else:
      continue
  return True

def replay():
  answer = input('Would you like to play again? (Y/N) ')
  if answer == 'Y':
    return True
  else:
    return False

while True:
  board = ['#', '_', '_', '_', '_', '_', '_', '_', '_', '_']

  player1_marker, player2_marker = player_input()

  turn = choose_first()
  print(turn + ' will go first')

  play_game = input('Are you ready to play? Enter "Y" or "N"')
  if play_game == 'Y':
    game_on = True
  else:
    game_on = False

  while game_on:
    if turn == 'Player 1':
      display_board(board)
      position = player_choice(board)
      place_marker(board, player1_marker, position)

      if win_check(board, player1_marker):
        display_board(board)
        print('Contratulations Player 1! You Won!')
        game_on = False
      else:
        if full_board_check(board):
          display_board(board)
          print('Game Over. Draw.')
          break
        else:
          print('\n'*3)
          turn = 'Player 2'
    else:
      display_board(board)
      position = player_choice(board)
      place_marker(board, player2_marker, position)

      if win_check(board, player2_marker):
        display_board(board)
        print('Contratulations Player 2! You Won!')
        game_on = False
      else:
        if full_board_check(board):
          display_board(board)
          print('Game Over. Draw.')
          break
        else:
          print('\n'*3)
          turn = 'Player 1'
  if not replay():
    break
```

---
