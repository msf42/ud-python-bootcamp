# Section 15: Advanced Python Modules

## Lesson 87: Collections Module: Counter

The Collections Module provides tools beyond set, list, dict, and tuple.

```py
from collections import Counter

# Counter with lists
mylist = [1, 2, 3, 4, 4, 4, 4, 3, 3]

Counter(mylist)
Out[3]: Counter({1: 1, 2: 1, 3: 3, 4: 4})

# Counter with strings
mystring="asdfadsgkkgoharei"

Counter(mystring)
Out[4]:
Counter({'a': 3,
      's': 2,
      'd': 2,
      'f': 1,
      'g': 2,
      'k': 2,
      'o': 1,
      'h': 1,
      'r': 1,
      'e': 1,
      'i': 1})

# Counter with words in sentences
mysent="I'm only faking when I get it right faking faking right"

Counter(mysent.split())
Out[7]:
Counter({"I'm": 1,
      'only': 1,
      'faking': 3,
      'when': 1,
      'I': 1,
      'get': 1,
      'it': 1,
      'right': 2})

# methods with Counter
c = Counter(mysent.split())

c.most_common(2)
Out[8]: [('faking', 3), ('right', 2)]
## Common Counter() patterns
sum(c.values())                 # total of all counts
c.clear()                       # reset all counts
list(c)                         # list unique elements
set(c)                          # convert to a set
dict(c)                         # convert to a regular dictionary
c.items()                       # convert to a list of (elem, cnt) pairs
Counter(dict(list_of_pairs))    # convert from a list of (elem, cnt) pairs
c.most_common()[:-n-1:-1]       # n least common elements
c += Counter()                  # remove zero and negative counts
```

---

## Lesson 88: Collections Module: defualtdict

A defaultdict will never raise a KeyError. Any key that does not exist gets the value returned by the default factory. Basically the same as a dict, but never gives a keyerror. Lambda functions are often used with them to set the default value.

```py
from collections import defaultdict
d = {'k1':1}
d['k1']
Out[11]: 1

d['k2']
Traceback (most recent call last):

  File "<ipython-input-12-b2441cdc6b89>", line 1, in <module>
  d['k2']

KeyError: 'k2'

d = defaultdict(object)
d['one']
Out[14]: <object at 0x7f000dbfdde0>

for item in d:
  print(item)

one

d = defaultdict(lambda : 0)

d['one']
Out[17]: 0

d['two']
Out[18]: 0

d
Out[19]: defaultdict(<function __main__.<lambda>()>, {'one': 0, 'two': 0})
```

---

## Lesson 89: Collections Module: ordereddict

Python 3.7 now keeps dictionaries ordered. However, ordereddict is still useful for 2 reasons:

- It ensures code will run properly if someone is using <3.7
- It communicates to others your *intention*

```py
print('Normal dictionary:')

d = {}

d['a'] = 1
d['b'] = 2
d['c'] = 3
d['e'] = 5
d['d'] = 4

for k, v in d.items():
  print(k, v)

Normal dictionary:
a 1
b 2
c 3
e 5
d 4

from collections import OrderedDict

print('OrderedDict:')

d = OrderedDict()

d['a'] = 1
d['b'] = 2
d['c'] = 3
d['e'] = 5
d['d'] = 4

for k, v in d.items():
  print(k, v)

OrderedDict:
a 1
b 2
c 3
e 5
d 4
```

---

## Lesson 90: Collections Module: namedtuple

A namedtuple is almost like creating a Class

```py
t = (1, 2, 3)

t[0]
Out[29]: 1

from collections import namedtuple

Dog = namedtuple('Dog', 'age breed name')

sam = Dog(age=2, breed='mutt', name='sam')

sam
Out[33]: Dog(age=2, breed='mutt', name='sam')

sam.age
Out[34]: 2

sam[0]
Out[35]: 2
```

---

## Lesson 91: Datetime

### Time

Only holds a value of time, not date associated with it.

```py
In [1]: import datetime
  ...:
  ...: t = datetime.time(4, 20, 1) # doesn't include microseconds (0 by default)
  ...:
  ...: # Let's show the different components
  ...: print(t)
  ...: print('hour :', t.hour)
  ...: print('minute:', t.minute)
  ...: print('second:', t.second)
  ...: print('microsecond:', t.microsecond)
  ...: print('tzinfo:', t.tzinfo) # timezone info
04:20:01
hour : 4
minute: 20
second: 1
microsecond: 0
tzinfo: None

In [2]: print('Earliest :', datetime.time.min)
  ...: print('Latest :', datetime.time.max)
  ...: print('Resolution:', datetime.time.resolution)
Earliest : 00:00:00
Latest : 23:59:59.999999
Resolution: 0:00:00.000001
```

### Date

```py
In [3]: today = datetime.date.today()
  ...: print(today)
  ...: print('ctime:', today.ctime())
  ...: print('tuple:', today.timetuple())
  ...: print('ordinal:', today.toordinal())
  ...: print('Year :', today.year)
  ...: print('Month:', today.month)
  ...: print('Day :', today.day)
2019-02-22
ctime: Fri Feb 22 00:00:00 2019
tuple: time.struct_time(tm_year=2019, tm_mon=2, tm_mday=22, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=53, tm_isdst=-1)
ordinal: 737112
Year : 2019
Month: 2
Day : 22

In [4]: print('Earliest :', datetime.date.min)
  ...: print('Latest :', datetime.date.max)
  ...: print('Resolution:', datetime.date.resolution)
Earliest : 0001-01-01
Latest : 9999-12-31
Resolution: 1 day, 0:00:00

In [5]: d1 = datetime.date(2015, 3, 11)
  ...: print('d1:', d1)
  ...:
  ...: d2 = d1.replace(year=1990)
  ...: print('d2:', d2)
d1: 2015-03-11
d2: 1990-03-11
```

### Arithmetic

```py
In [6]: d1
Out[6]: datetime.date(2015, 3, 11)

In [7]: d2
Out[7]: datetime.date(1990, 3, 11)

In [8]: d1-d2
Out[8]: datetime.timedelta(days=9131)
```

---

## Lesson 92: Python Debugger - pdb

### Without the debugger (trying to add an int and str)

```py
In [1]: x = [1,3,4]
  ...: y = 2
  ...: z = 3
  ...:
  ...: result = y + z
  ...: print(result)
  ...: result2 = y+x
  ...: print(result2)
5
Traceback (most recent call last):

File "<ipython-input-1-000ad75e98f1>", line 7, in <module>
result2 = y+x

TypeError: unsupported operand type(s) for +: 'int' and 'list'
```

### Using the debugger

```py
In [3]: import pdb
  ...:
  ...: x = [1,3,4]
  ...: y = 2
  ...: z = 3
  ...:
  ...: result = y + z
  ...: print(result)
  ...:
  ...: # Set a trace using Python Debugger
  ...: pdb.set_trace()
  ...:
  ...: result2 = y+x
  ...: print(result2)
5
--Return--
None
> <ipython-input-3-6c36e8161fda>(11)<module>()
9
10 # Set a trace using Python Debugger. Do this right before likely error
---> 11 pdb.set_trace()
12
13 result2 = y+x


ipdb>                       # the debugger is interactive
ipdb> x
[1, 3, 4]

ipdb> y
2

ipdb> y + z
5

ipdb> y**2
4

ipdb> y + x
*** TypeError: unsupported operand type(s) for +: 'int' and 'list'

ipdb> q
Traceback (most recent call last):

File "<ipython-input-3-6c36e8161fda>", line 11, in <module>
pdb.set_trace()

File "C:\Users\Steve\Desktop\WPy-3710\python-3.7.1.amd64\lib\bdb.py", line 92, in trace_dispatch
return self.dispatch_return(frame, arg)

File "C:\Users\Steve\Desktop\WPy-3710\python-3.7.1.amd64\lib\bdb.py", line 154, in dispatch_return
if self.quitting: raise BdbQuit

BdbQuit
```

---

## Lesson 93: Timing Your Code - timeit

```py
import timeit
```

Let's use `timeit` to time various methods of creating the string '0-1-2-3-.....-99'
We'll pass two arguments: the actual line we want to test encapsulated as a string and the number of times we wish to run it. Here we'll choose 10,000 runs to get some high enough numbers to compare various methods.

```py
import timeit

timeit.timeit('"-".join(str(n) for n in range(100))', number=10000)
Out[2]: 0.25259609800014005

# List comprehension
timeit.timeit('"-".join([str(n) for n in range(100)])', number=10000)
Out[3]: 0.21624181300012424

# Map()
timeit.timeit('"-".join(map(str, range(100)))', number=10000)
Out[4]: 0.18406770499950653
```

---

## Lesson 94: Regular Expressions - re

Regular expressions are text-matching patterns described with a formal syntax. You'll often hear regular expressions referred to as 'regex' or 'regexp' in conversation. Regular expressions can include a variety of rules, from finding repetition, to text-matching, and much more. As you advance in Python you'll see that a lot of your parsing problems can be solved with regular expressions (they're also a common interview question!).

### Searching for Patterns in Text

```py
import re

patterns = ['term1', 'term2']

text = 'this is a string with term1 only'

re.search('hello', 'hello world')
Out[4]: <re.Match object; span=(0, 5), match='hello'>
# this shows where it was found, and what matched

for pattern in patterns:
  print('Searching for "%s" in:\n "%s"\n' %(pattern,text))

# Check for match
  if re.search(pattern,text):   # if a match was found
    print('Match was found. \n')
  else:
    print('No Match was found.\n')

Searching for "term1" in:
  "this is a string with term1 only"

Match was found.

Searching for "term2" in:
  "this is a string with term1 only"

No Match was found.

# List of patterns to search for
pattern = 'term1'

# Text to parse
text = 'This is a string with term1, but it does not have the other term.'

match = re.search(pattern,text)

type(match)
Out[6]: re.Match

# Show start of match
match.start()
Out[7]: 22  # index of start

# Show end
match.end()
Out[8]: 27  # index of end
```

### Split with Regular Expressions

```py
# Term to split on
split_term = '@'

phrase = 'What is the domain name of someone with the email: hello@gmail.com'

# Split the phrase
re.split(split_term,phrase)
Out[9]: ['What is the domain name of someone with the email: hello', 'gmail.com']
```

### Finding All Instances of a Pattern

```py
# Returns a list of all matches
re.findall('match','test phrase match is in middle')
Out[10]: ['match']

re.findall('happy', 'I am happy. Are you happy?')
Out[11]: ['happy', 'happy']
```

### re Pattern Syntax

This will be the bulk of this lecture on using re with Python. Regular expressions support a huge variety of patterns beyond just simply finding where a single string occurred.
We can use *metacharacters* along with re to find specific types of patterns.
Since we will be testing multiple re syntax forms, let's create a function that will print out results given a list of various regular expressions and a phrase to parse:

```py
def multi_re_find(patterns,phrase):
  '''
  Takes in a list of regex patterns
  Prints a list of all matches
  '''
  for pattern in patterns:
    print('Searching the phrase using the re check: %r' %(pattern))
    print(re.findall(pattern,phrase))
    print('\n')
```

### Repetition Syntax

There are five ways to express repetition in a pattern:

1. A pattern followed by the meta-character `*` is repeated zero or more times.
2. Replace the `*` with `+` and the pattern must appear at least once.
3. Using `?` means the pattern appears zero or one time.
4. For a specific number of occurrences, use `{m}` after the pattern, where **m** is replaced with the number of times the pattern should repeat.
5. Use `{m,n}` where **m** is the minimum number of repetitions and **n** is the maximum. Leaving out **n** `{m,}` means the value appears at least **m** times, with no maximum.

Now we will see an example of each of these using our multi_re_find function:

```py
test_phrase = 'sdsd..sssddd...sdddsddd...dsds...dsssss...sdddd'

test_patterns = [ 'sd*',     # s followed by zero or more d's
        'sd+',          # s followed by one or more d's
        'sd?',          # s followed by zero or one d's
        'sd{3}',        # s followed by three d's
        'sd{2,3}',      # s followed by two to three d's
        ]

multi_re_find(test_patterns,test_phrase)
Searching the phrase using the re check: 'sd*'
['sd', 'sd', 's', 's', 'sddd', 'sddd', 'sddd', 'sd', 's', 's', 's', 's', 's', 's', 'sdddd']

Searching the phrase using the re check: 'sd+'
['sd', 'sd', 'sddd', 'sddd', 'sddd', 'sd', 'sdddd']

Searching the phrase using the re check: 'sd?'
['sd', 'sd', 's', 's', 'sd', 'sd', 'sd', 'sd', 's', 's', 's', 's', 's', 's', 'sd']

Searching the phrase using the re check: 'sd{3}'
['sddd', 'sddd', 'sddd', 'sddd']

Searching the phrase using the re check: 'sd{2,3}'
['sddd', 'sddd', 'sddd', 'sddd']
```

### Character Sets

```py
test_phrase = 'sdsd..sssddd...sdddsddd...dsds...dsssss...sdddd'

test_patterns = ['[sd]',    # either s or d
        's[sd]+']   # s followed by one or more s or d

multi_re_find(test_patterns,test_phrase)
Searching the phrase using the re check: '[sd]'
['s', 'd', 's', 'd', 's', 's', 's', 'd', 'd', 'd', 's', 'd', 'd', 'd', 's', 'd', 'd', 'd', 'd', 's', 'd', 's', 'd', 's', 's', 's', 's', 's', 's', 'd', 'd', 'd', 'd']


Searching the phrase using the re check: 's[sd]+'
['sdsd', 'sssddd', 'sdddsddd', 'sds', 'sssss', 'sdddd']
```

### Exclusion

We can use `^` to exclude terms by incorporating it into the bracket syntax notation. For example: `[^...]` will match any single character not in the brackets. Let's see some examples:

```py
test_phrase = 'This is a string! But it has punctuation. How can we remove it?'
```

Use `[^!.? ]` to check for matches that are not a !,.,?, or space. Add a `+` to check that the match appears at least once. This basically translates into finding the words.

```py
re.findall('[^!.? ]+',test_phrase)
Out[16]:
['This',
  'is',
  'a',
  'string',
  'But',
  'it',
  'has',
  'punctuation',
  'How',
  'can',
  'we',
  'remove',
  'it']
```

### Character Ranges

```py
test_phrase = 'This is an example sentence. Lets see if we can find some letters.'

test_patterns=['[a-z]+',      # sequences of lower case letters
        '[A-Z]+',      # sequences of upper case letters
        '[a-zA-Z]+',   # sequences of lower or upper case letters
        '\[A-Z\][a-z]+'] # one upper case letter followed by lower case letters

multi_re_find(test_patterns,test_phrase)
Searching the phrase using the re check: '[a-z]+'
['his', 'is', 'an', 'example', 'sentence', 'ets', 'see', 'if', 'we', 'can', 'find', 'some', 'letters']

Searching the phrase using the re check: '[A-Z]+'
['T', 'L']

Searching the phrase using the re check: '[a-zA-Z]+'
['This', 'is', 'an', 'example', 'sentence', 'Lets', 'see', 'if', 'we', 'can', 'find', 'some', 'letters']

Searching the phrase using the re check: '\[A-Z\][a-z]+'
['This', 'Lets']
```

### Escape Codes

| **Code** | **Meaning**                            |
| -------- | -------------------------------------- |
| \d       | a digit                                |
| \D       | a non-digit                            |
| \s       | whitespace (tab, space, newline, etc.) |
| \S       | non-whitespace                         |
| \w       | alphanumeric                           |
| \W       | non-alphanumeric                       |

```py
test_phrase = 'This is a string with some numbers 1233 and a symbol #hashtag'

test_patterns=[ r'\d+', # sequence of digits
        r'\D+', # sequence of non-digits
        r'\s+', # sequence of whitespace
        r'\S+', # sequence of non-whitespace
        r'\w+', # alphanumeric characters
        r'\W+', # non-alphanumeric
        ]

multi_re_find(test_patterns,test_phrase)
Searching the phrase using the re check: '\\d+'
['1233']


Searching the phrase using the re check: '\\D+'
['This is a string with some numbers ', ' and a symbol #hashtag']


Searching the phrase using the re check: '\\s+'
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']


Searching the phrase using the re check: '\\S+'
['This', 'is', 'a', 'string', 'with', 'some', 'numbers', '1233', 'and', 'a', 'symbol', '#hashtag']


Searching the phrase using the re check: '\\w+'
['This', 'is', 'a', 'string', 'with', 'some', 'numbers', '1233', 'and', 'a', 'symbol', 'hashtag']


Searching the phrase using the re check: '\\W+'
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' #']
```

---

## Lesson 95: StringIO

This has been changed to the `io` module in newer versions of python.
It basically just allows a string to be treated like a file. Rarely used, except when taking data from the web.

```py
import io

message = 'this is a normal string'

f = io.StringIO(message)

# now f will be treated like a file

f.read()
Out[8]: 'this is a normal string'

f.write('this is a second line')

f.seek(0)
Out[12]: 0

f.read()
Out[13]: 'this is a normal stringthis is a second line'
```

---

## Lesson 96: Advanced Python Modules FAQ

Nothing to see here.

---
