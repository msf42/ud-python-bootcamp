# Section 08: Object Oriented Programming

---

## 59 - Object Oriented Programming - Introduction

### Basic format:

```py
# use Uppercase for Classes
# looks like a function, but actually called a method when inside a Class

class NameOfClass():
   def __init__(self, param1, param2):     # pass in a parameter here
    self.param1 = param1        # assign it to an attribute of the function here
    self.param2 = param2

   def some_method(self):          # after above, we can have any number of methods
    # perform some action
    print(self.param1)
```

---

## 60 - Object Oriented Programming - Attributes and Class Keyword

### A class is a blueprint for a future object.

Each word of a ClassName is capitalized
As opposed to other objects, which use lower_case

```py
In [1]: mylist = [1, 2, 3]           # a simple list

In [2]: myset = set()                # a simple set

In [3]: type(myset)                  # what is the type? a set
Out[3]: set

In [4]: type(mylist)
Out[4]: list

   # class can be used to create a user-defined object
   # it creates a blueprint for a future object
   # from a class, we can create an "instance" of a future object
   # an instance is a specific object created from a class

In [5]: class Sample():
...:     pass        # a placeholder: don't do anything yet
...:

In [6]: my_sample = Sample()    # creates an instance of the class

In [7]: type(my_sample)            # what type is my_sample?
Out[7]: __main__.Sample            # it is an insance of the class Sample

In [8]: class Dog():               # always use Campbell casing
...:     def __init__(self,breed):  
          # first method is always a special method called __init__
          # self keyword is a reference to an instance of the class
          # what kind of attributes do dogs have? breed is one
...:         self.breed = breed
          # it tells python we will be passing these attributes
...:

In [9]: my_dog = Dog()                # 1 argument was expected (breed)
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-9-b867e62b00ec> in <module>()
----> 1 my_dog = Dog()

TypeError: __init__() missing 1 required positional argument: 'breed'

# error above, because 'breed' parameter was expected

In [10]: my_dog = Dog(breed = 'Lab')

In [11]: type(my_dog)
Out[11]: __main__.Dog

In [12]: my_dog.breed    # now breed is an attribute of my_dog
Out[12]: 'Lab'

# init is a constructor for the class; it is called automatically when an instance of the class is created

In [17]: class Dog():
   ...:     def __init__(self,mybreed):
      # by convention, these are the same, but changing to show how it works
   ...:     # Attributes
   ...:     # We take in the argument (mybreed)
   ...:     # Assign it using self.attribute_name (assign mybreed to self.breed)
   ...:         self.breed = mybreed
   ...:

In [18]: my_dog = Dog(mybreed = 'Huskie')
      # now 'mybreed' must be passed, so it will be returned with self.breed
      # but best to use the same word for all 3; it makes it easier

In [19]: type(my_dog)
Out[19]: __main__.Dog

In [20]: my_dog.breed         # still expects '.breed'
Out[20]: 'Huskie'

In [21]: class Dog():         # adding some more attributes
   ...:     def __init__(self,breed,name,spots):
   ...:         self.breed = breed
         # "self" so that when we define 'my_dog' using the Dog class
                  # we can type my_dog.breed
   ...:         self.name = name      # we can type my_dog.name
   ...:
   ...:         # Expect boolean True/False; attributes can be anything
   ...:         self.spots = spots
   ...:

In [22]: my_dog = Dog(breed='lab', name='Sammy', spots=False)

   # now breed, name, and spots are all attributes of Dog

In [23]: my_dog.breed
Out[23]: 'lab'

In [24]: my_dog.name
Out[24]: 'Sammy'

In [25]: my_dog.spots
Out[25]: False
```

---

## 61 - Object Oriented Programming - Class Object Attributes and Methods

```py
In [1]: class Dog():
...:
...:     # Class Object Attribute
...:     # Same for any instance of a class
...:     # Therefore "self" is not needed
...:     species = 'mammal'
...:
...:     def __init__(self,breed,name,spots):
...:         self.breed = breed
...:         self.name = name
...:
...:         # Expect boolean True/False
...:         self.spots = spots
...:

In [2]: my_dog = Dog(breed = 'Lab', name = 'Sam', spots = False)

In [3]: type(my_dog)
Out[3]: __main__.Dog

In [4]: my_dog.species
    # species attribute is available, even though we didn’t specifically
Out[4]: 'mammal'              # define it in __init__

In [8]: class Dog():
...:     species = 'mammal'
...:
...:     def __init__(self,breed,name):
...:         self.breed = breed
...:         self.name = name
...:     # Methods are functions defined inside the Class
...:    # Methods act on an object, taking the object itself into account, through              the use of ‘self’ keyword
...:     # Methods are Operations/Actions, whereas attributes are characteristics
...:     def bark(self):
...:         print("WOOF!")
...:

In [9]: my_dog = Dog('Lab', 'Frankie')

In [10]: my_dog.species
Out[10]: 'mammal'

In [11]: my_dog.name      # an attribute, so no ()
Out[11]: 'Frankie'

In [12]: my_dog.bark()    # Note we call the Method with (), unlike attributes
WOOF!                     # because we execute methods, but not attributes

In [13]: class Dog():
   ...:     species = 'mammal'
   ...:
   ...:     def __init__(self,breed,name):
   ...:         self.breed = breed
   ...:         self.name = name
   ...:
   ...:     def bark(self):
   ...:         print("WOOF! My name is {}".format(self.name))
   ...:         # Note we must say self.name
   ...:

In [14]: my_dog = Dog('Lab', 'Frankie')

In [15]: my_dog.bark()
WOOF! My name is Frankie

In [16]: class Dog():
   ...:     species = 'mammal'
   ...:
   ...:     def __init__(self,breed,name):
   ...:         self.breed = breed
   ...:         self.name = name
   ...:
   ...:     def bark(self, number):
   ...:         print("WOOF! My name is {} and the number is {}".format(self.name, number))
   ...:         # Number is provided by ‘bark’, so ‘self.’ is not required
   ...:

In [17]: my_dog = Dog('Lab', 'Frankie')

In [18]: my_dog.bark(14)
WOOF! My name is Frankie and the number is 14

In [22]: class Circle():
   ...:
   ...:     # Class Object Attribute is something that is true
      # for any instance of the class (such as value of pi)
   ...:     pi = 3.14
   ...:
   ...:     def __init__(self, radius=1):  # 1 is default value
   ...:
   ...:         self.radius = radius
   ...:
   ...:     # Method
   ...:     def get_circumference(self):
   ...:         return self.radius * self.pi * 2
   ...:

In [23]: my_circle = Circle()

In [24]: my_circle.pi
Out[24]: 3.14

In [25]: my_circle.radius
Out[25]: 1    # default value above

In [27]: my_circle.get_circumference()
Out[27]: 6.28

In [28]: class Circle():
   ...:
   ...:     pi = 3.14
   ...:
   ...:     def __init__(self, radius=1):
   ...:
   ...:         self.radius = radius
   ...:         self.area = radius*radius*self.pi  # we can calculate attribute
   ...:       # We could actually say Circle.pi, since it is a Class Object Attribute
   ...:
   ...:     def get_circumference(self):
   ...:         return self.radius * self.pi * 2
   ...:

In [29]: my_circle = Circle(30)

In [30]: my_circle.area
Out[30]: 2826.0

In [32]: my_circle.get_circumference()
Out[32]: 188.4
```

---

## 62 - Object Oriented Programming - Inheritance and Polymorphism

### Inheritance

```py
In [1]: class Animal():
...:
...:     def __init__(self):
...:         print("Animal Created")
...:

In [2]: myanimal = Animal()  # Using the Animal Class
Animal Created

In [3]: class Animal():      # Adding some more features
...:
...:     def __init__(self):
...:         print("Animal Created")
...:
...:     def who_am_i(self):
...:         print("I am an animal")
...:
...:     def eat(self):
...:         print("I am eating")
...:

In [4]: myanimal = Animal()
Animal Created

In [5]: myanimal.eat()
I am eating

In [6]: myanimal.who_am_i()
I am an animal

In [7]: class Dog(Animal):   # Using the Animal Class to create Dog class
...:
...:     def __init__(self):  # If Dog is called
...:         Animal.__init__(self)  # call animal.self first
...:         print("Dog Created")   # then print Dog Created
...:

In [8]: mydog = Dog()  # Now Dog does what is defined in both Dog and Animal
Animal Created
Dog Created

In [9]: mydog.who_am_i()
I am an animal

In [10]: mydog.eat()
I am eating

In [11]: class Dog(Animal):
   ...:
   ...:     def __init__(self):
   ...:         Animal.__init__(self)
   ...:         print("Dog Created")

   ...:     def who_am_i(self):
     # We can overwrite a characteristic of Animal if we define it here
   ...:         print("I am a dog!")
   ...:

In [12]: mydog = Dog()
Animal Created
Dog Created

In [13]: mydog.eat()
I am eating

In [14]: mydog.who_am_i()
I am a dog!

In [15]: class Dog(Animal):
   ...:
   ...:     def __init__(self):
   ...:         Animal.__init__(self)
   ...:         print("Dog Created")

   ...:     def who_am_i(self):
   ...:         print("I am a dog!")

   ...:     def bark(self):   # We can also add a new method
   ...:         print("Woof!")
   ...:

In [16]: mydog = Dog()
Animal Created
Dog Created
# Now we have both the Animal methods and Dog methods
In [17]: mydog.bark()
Woof!
```

### Polymorphism

```py
In [19]: class Dog():                   # defining Dog again
   ...:
   ...:     def __init__(self, name):
   ...:         self.name = name

   ...:     def speak(self):
   ...:         return self.name + " says woof!"
   ...:

In [20]: class Cat():                   # and Cat
   ...:
   ...:     def __init__(self, name):
   ...:         self.name = name

   ...:     def speak(self):
   ...:         return self.name + " says meow!"
   ...:

In [21]: niko = Dog("niko")  # Niko is an instance of Dog with the name Niko

In [22]: felix = Cat("felix")

# Niko is a Dog, so .speak = woof; Felix is a Cat, so .speak = meow
In [23]: print(niko.speak())
niko says woof!

In [24]: print(felix.speak())
felix says meow!

# We can put these in a for loop. Calling .speak for both will give a different result
In [25]: for pet in [niko, felix]:
   ...:     print(type(pet))
   ...:     print(pet.speak())
   ...:
<class '__main__.Dog'>
niko says woof!
<class '__main__.Cat'>
felix says meow!

# Or we can put them in a function. The function will produce different results, depending on the ‘pet’
In [26]: def pet_speak(pet):
   ...:     print(pet.speak())
   ...:

In [27]: pet_speak(niko)
niko says woof!

In [28]: pet_speak(felix)
felix says meow!

In [29]: class Animal():  # This is a class to build other classes, we never plan on using it
   ...:     def __init__(self, name):
   ...:         self.name = name

   ...:     def speak(self):  # So we generate an error if Animal is defined
   ...:         raise NotImplementedError("Subclass must implement this abstract method")
   ...:

In [30]: myanimal = Animal('fred')

In [31]: myanimal.speak()   # Error because Animal is only to be used to build other Classes
Traceback (most recent call last):

   File "<ipython-input-31-188713a00ef6>", line 1, in <module>
    myanimal.speak()

   File "<ipython-input-29-62bd96e98f21>", line 5, in speak
    raise NotImplementedError("Subclass must implement this abstract method")

   NotImplementedError: Subclass must implement this abstract method

In [32]: class Dog(Animal):
   ...:     def speak(self):
   ...:         return self.name + " says woof!"
   ...:

In [33]: class Cat(Animal):
   ...:     def speak(self):
   ...:         return self.name + " says meow!"
   ...:

In [34]: fido = Dog("Fido")

In [35]: isis = Cat("Isis")

In [36]: fido.speak()
Out[36]: 'Fido says woof!'

In [37]: isis.speak()
Out[37]: 'Isis says meow!'
```

---

## 63 - Object Oriented Programming - Special (Magic/Dunder) Methods

```py
In [1]: mylist = [1, 2, 3]

In [2]: len(mylist)  
   # built-in functions like len or print are available to lists, strings, etc
Out[2]: 3            # we need to make them available to Methods

In [3]: class Sample():             # a simple Class
...:     pass
...:

In [4]: mysample = Sample()

In [5]: len(mysample)         # len is unavailable, unless we make it
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-5-97b353a42cb3> in <module>()
----> 1 len(mysample)

TypeError: object of type 'Sample' has no len()

In [6]: print(mysample)             # printing only shows its location
<__main__.Sample object at 0x7f50fd330c50>

In [7]: class Book():               # a class for Books
...:     def __init__(self, title, author, pages):
...:         self.title = title
...:         self.author = author
...:         self.pages = pages
...:

In [8]: b = Book('Python Rocks', 'Jose', 200)     # define 'b' as a book with these attributes

In [9]: print(b)
<__main__.Book object at 0x7f50fd2a17b8>

In [11]: class Book():
   ...:     def __init__(self, title, author, pages):
   ...:         self.title = title
   ...:         self.author = author
   ...:         self.pages = pages

   ...:     def __str__(self):  
      # here we use a special method for any strings in our class
   ...:         return f"{self.title} by {self.author}"
   ...:

In [13]: b = Book('Python Rocks', 'Jose', 200)

In [14]: print(b)           # now it prints
Python Rocks by Jose

In [15]: class Book():
   ...:     def __init__(self, title, author, pages):
   ...:         self.title = title
   ...:         self.author = author
   ...:         self.pages = pages

   ...:     def __str__(self):
   ...:         return f"{self.title} by {self.author}"

   ...:     def __len__(self):                # now we define length
   ...:         return self.pages
   ...:

In [16]: b = Book('Python Rocks', 'Jose', 200)

In [17]: len(b)                               # now len works
Out[17]: 200

In [18]: del b                                # this deletes a variable

In [19]: b
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-19-89e6c98d9288> in <module>()
----> 1 b

NameError: name 'b' is not defined

In [20]: class Book():
   ...:     def __init__(self, title, author, pages):
   ...:         self.title = title
   ...:         self.author = author
   ...:         self.pages = pages

   ...:     def __str__(self):
   ...:         return f"{self.title} by {self.author}"

   ...:     def __len__(self):
   ...:         return self.pages

   ...:     def __del__(self):
   ...:         print("A Book Object has been deleted")
   ...:

In [21]: b = Book('Python Rocks', 'Jose', 200)

In [22]: print(b)
Python Rocks by Jose

In [23]: len(b)
Out[23]: 200

In [24]: del b                             # now when we delete we get the message
A Book Object has been deleted
```

---

## 64/65 - Homework

### Problem 1

```py
import math

class Line:

   def __init__(self,coor1,coor2):
    self.coor1 = coor1
    self.coor2 = coor2
    print(f"coordinate1 = {coor1}")
    print(f"coordinate2 = {coor2}")

   def distance(self):
    self.xdiff = self.coor1[0] - self.coor2[0]    # difference in x values
    self.ydiff = self.coor1[1] - self.coor2[1]    # difference in y values
    self.dist = math.sqrt((self.xdiff **2) + (self.ydiff ** 2))  
     # distance formula
    print(f"distance = {self.dist}")

   def slope(self):
    self.rise = self.coor2[1] - self.coor1[1]
     # difference in y values (rise)
    self.run = self.coor2[0] - self.coor1[0]
     # difference in x values (run)
    self.slope = self.rise/self.run      # slope formula
    print(f"slope = {self.slope}")

li = Line((3, 2), (8, 10))

li.distance()

li.slope()
```

### Problem 1 Output

```py
coordinate1 = (3, 2)
coordinate2 = (8, 10)
distance = 9.433981132056603
slope = 1.6
```

### Problem 2

```py
class Cylinder:

    pi = 3.14159

    def __init__(self,height=1,radius=1):
         self.height = height
         self.radius = radius

    def volume(self):
         self.volume = (self.pi * self.radius ** 2) * self.height
         print(f"Given a height of {self.height} and a radius of {self.radius}, the volume is {self.volume}")

    def surface_area(self):
         self.area = ((self.pi * (self.radius ** 2)) * 2) + (self.height * (self.pi * self.radius * 2))
         print(f"Given a height of {self.height} and a radius of {self.radius}, the surface area is {self.area}")

c = Cylinder()

c.volume()

c.surface_area()

c = Cylinder(7, 12)

c.volume()

c.surface_area()
```

### Problem 2 Output

```py
steve@steve-lubu:~/Drive/_ACTIVE/Tech/Python Bootcamp$ python3.6 08-64b.py
Given a height of 1 and a radius of 1, the volume is 3.14159
Given a height of 1 and a radius of 1, the surface area is 12.56636
Given a height of 7 and a radius of 12, the volume is 3166.7227199999998
Given a height of 7 and a radius of 12, the surface area is 1432.56504
```

---

## 66/67 - Challenge

```py
class Account:

   def __init__(self, owner, balance):
      self.owner = owner
      self.balance = balance

   def __str__(self):
      return f"Account Owner:{self.owner}\nAccount Balance:{self.balance}"

   def make_deposit(self, deposit):
      self.deposit = deposit
      self.balance = self.balance + self.deposit
      print(f"Deposit Accepted.\nNew Balance = {self.balance}")

   def make_withdraw(self, withdraw):
      self.withdraw = withdraw
      if self.withdraw > self.balance:
           print(f"Funds Unavailable! Your balance is only {self.balance}")
      else:
           self.balance = self.balance - self.withdraw
           print(f"Withdraw of {self.withdraw} accepted\nNew Balance = {self.balance}")
```

### Running the script:

```py
In [1]: acct1 = Account('Jose', 100)

In [1]: print(acct1)
Account Owner: Jose
Account Balance: 100

In [2]: acct1.make_deposit(50)
Deposit Accepted.
New Balance = 150

In [3]: acct1.make_withdraw(100)
Withdraw of 100 accepted
New Balance = 50

In [5]: acct1.make_deposit(500)
Deposit Accepted.
New Balance = 550

In [6]: acct1.make_withdraw(1000)
Funds Unavailable! Your balance is only 550
```

---
